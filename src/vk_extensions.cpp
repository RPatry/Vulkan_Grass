#include "vk_extensions.hpp"
#include <vector>
#include <iostream>
#include <algorithm>
#include <ranges>

void Test_Show_Global_Extensions_Properties() noexcept {
    unsigned int ext_count {};
    vkEnumerateInstanceExtensionProperties(nullptr, &ext_count, nullptr);
    std::vector<VkExtensionProperties> extensions (ext_count);
    vkEnumerateInstanceExtensionProperties(nullptr, &ext_count, extensions.data());
    for (auto const& ext: extensions) {
        std::cout << "\t" << ext.extensionName << std::endl;
    }
}


std::vector<std::string> Get_All_Extensions_Names_For_Device(VkPhysicalDevice device) {
    std::vector<std::string> result {};
    std::vector<VkExtensionProperties> props {};
    uint32_t ext_count {};
    vkEnumerateDeviceExtensionProperties(device, nullptr, &ext_count, nullptr);
    props.resize(ext_count);
    vkEnumerateDeviceExtensionProperties(device, nullptr, &ext_count, props.data());
    auto view_extension_name_strings = props | std::views::transform([](auto const& prop) { return std::string { prop.extensionName }; });
    return std::vector<std::string> { view_extension_name_strings.begin(), view_extension_name_strings.end() };
}

bool Check_Device_Has_All_Extensions(VkPhysicalDevice device) {
    const auto extensions { Get_All_Extensions_Names_For_Device(device) };
    std::set<std::string> unique_ext { extensions.begin(), extensions.end() },
        set_device_extensions { device_extensions<std::string>.begin(), device_extensions<std::string>.end() },
        inter {};
    set_intersection(unique_ext.begin(), unique_ext.end(), set_device_extensions.begin(), set_device_extensions.end(), inserter(inter, inter.end()));

#ifdef DEBUG_MODE
    std::cout << std::endl << "Extensions of the device:" << std::endl;
    for (auto& ext: unique_ext) {
        std::cout << ext << std::endl;
    }
#endif

    return inter == set_device_extensions;
}
