#include "vk_image_helpers.hpp"
#include "vk_exceptions.hpp"

VkImage Create_Simple_2D_VkImage(VkDevice logical_device, size_t width, size_t height, VkFormat format) {
    VkImageCreateInfo image_cinfo {};
    image_cinfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_cinfo.pNext = nullptr;
    image_cinfo.flags = 0;
    image_cinfo.imageType = VK_IMAGE_TYPE_2D;
    image_cinfo.format = format;
    image_cinfo.extent.width = width;
    image_cinfo.extent.height = height;
    image_cinfo.extent.depth = 1;
    image_cinfo.mipLevels = 1;
    image_cinfo.arrayLayers = 1;
    image_cinfo.samples = VK_SAMPLE_COUNT_1_BIT;
    image_cinfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_cinfo.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;
    image_cinfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_cinfo.queueFamilyIndexCount = 0;
    image_cinfo.pQueueFamilyIndices = nullptr;
    image_cinfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;

    VkImage image {};
    const VkResult res_create_image { vkCreateImage(logical_device, &image_cinfo, nullptr, &image) };
    Throw_On_VkResult_Error("Could not create VkImage for texture", res_create_image);

    return image;
}

VkImageView Create_Simple_2D_VkImageView(VkDevice logical_device, VkImage image, VkFormat format) {
    VkImageViewCreateInfo image_view_cinfo {};
    image_view_cinfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_cinfo.flags = 0;
    image_view_cinfo.image = image;
    image_view_cinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_cinfo.format = format;
    image_view_cinfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_cinfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_cinfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_cinfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_cinfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    image_view_cinfo.subresourceRange.layerCount = 1;
    image_view_cinfo.subresourceRange.levelCount = 1;
    image_view_cinfo.subresourceRange.baseMipLevel = 0;
    image_view_cinfo.subresourceRange.baseArrayLayer = 0;

    VkImageView image_view {};
    const VkResult res_create_image_view { vkCreateImageView(logical_device, &image_view_cinfo, nullptr, &image_view) };
    Throw_On_VkResult_Error("Could not create VkImageView for texture", res_create_image_view);

    return image_view;
}

VkDescriptorImageInfo Get_VkDescriptorImageInfo_For_Shader(VkImageView image_view, VkSampler sampler) {
    VkDescriptorImageInfo info {};
    info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    info.imageView = image_view;
    info.sampler = sampler;
    return info;
}
