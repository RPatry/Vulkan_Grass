 # Store all shaders directly inside the executable

	.section .rodata
	.global Static_Patch_Blade_Frag_Shader
	.align 8
Static_Patch_Blade_Frag_Shader:
	.incbin "bin/shaders/static_blade.frag.spv"
Static_Patch_Blade_Frag_Shader_End:	

	.global Static_Patch_Blade_Vert_Shader
	.align 8
Static_Patch_Blade_Vert_Shader:
	.incbin "bin/shaders/static_blade.vert.spv"
Static_Patch_Blade_Vert_Shader_End:

	.section .rodata
	.global Static_Patch_Ground_Frag_Shader
	.align 8
Static_Patch_Ground_Frag_Shader:
	.incbin "bin/shaders/static_ground.frag.spv"
Static_Patch_Ground_Frag_Shader_End:

	.global Static_Patch_Ground_Vert_Shader
	.align 8
Static_Patch_Ground_Vert_Shader:
	.incbin "bin/shaders/static_ground.vert.spv"
Static_Patch_Ground_Vert_Shader_End:

	.global Static_Patch_Blade_Frag_Shader_Size
	.align 8
Static_Patch_Blade_Frag_Shader_Size:
	.int Static_Patch_Blade_Frag_Shader_End - Static_Patch_Blade_Frag_Shader

	.global Static_Patch_Blade_Vert_Shader_Size
	.align 8
Static_Patch_Blade_Vert_Shader_Size:
	.int Static_Patch_Blade_Vert_Shader_End - Static_Patch_Blade_Vert_Shader

	.global Static_Patch_Ground_Frag_Shader_Size
	.align 8
Static_Patch_Ground_Frag_Shader_Size:
	.int Static_Patch_Ground_Frag_Shader_End - Static_Patch_Ground_Frag_Shader

	.global Static_Patch_Ground_Vert_Shader_Size
	.align 8
Static_Patch_Ground_Vert_Shader_Size:
	.int Static_Patch_Ground_Vert_Shader_End - Static_Patch_Ground_Vert_Shader

	.align 8
Ground_Frag_Shader:
	.incbin "bin/shaders/ground.frag.spv"
Ground_Frag_Shader_End:	

	.global Ground_Vert_Shader
	.align 8
Ground_Vert_Shader:
	.incbin "bin/shaders/ground.vert.spv"
Ground_Vert_Shader_End:

	.global Blade_Frag_Shader_Size
	.align 8
Blade_Frag_Shader_Size:
	.int Blade_Frag_Shader_End - Blade_Frag_Shader

	.global Blade_Vert_Shader_Size
	.align 8
Blade_Vert_Shader_Size:
	.int Blade_Vert_Shader_End - Blade_Vert_Shader

	.global Ground_Frag_Shader_Size
	.align 8
Ground_Frag_Shader_Size:
	.int Ground_Frag_Shader_End - Ground_Frag_Shader

	.global Ground_Vert_Shader_Size
	.align 8
Ground_Vert_Shader_Size:
	.int Ground_Vert_Shader_End - Ground_Vert_Shader
