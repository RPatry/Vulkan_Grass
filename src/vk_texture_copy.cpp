#include "vk_texture_copy.hpp"
#include <numeric>
#include <ranges>
#include <optional>
#include "math.hpp"
#include "vk_exceptions.hpp"
#include "vk_queuefamily.hpp"
#include "vk_synchronisation.hpp"

namespace {
void Create_Command_Buffer_And_Pool(VkDevice logical_device, VkCommandPool& command_pool, VkCommandBuffer& command_buffer, unsigned int index_queue_family) {
    VkCommandPoolCreateInfo pool_cinfo {};
    pool_cinfo.sType  = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    pool_cinfo.queueFamilyIndex = index_queue_family;
    pool_cinfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;

    const VkResult pool_creation_res { vkCreateCommandPool(logical_device, &pool_cinfo, nullptr, &command_pool) };
    Throw_On_VkResult_Error("Command pool creation failed, cannot transfer textures to GPU memory", pool_creation_res);

    VkCommandBufferAllocateInfo alloc_info {};
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.commandPool = command_pool;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = 1;
    const VkResult alloc_res { vkAllocateCommandBuffers(logical_device, &alloc_info, &command_buffer) };
    Throw_On_VkResult_Error("Command buffer creation failure, cannot transfer textures to GPU memory", alloc_res);
}
}

void Vulkan_Texture_Copy_Job_Scheduler::Set_Device(VkPhysicalDevice physical_device, VkDevice logical_device, VkSurfaceKHR surface_khr) {
    this->physical_device = physical_device;
    this->logical_device = logical_device;
    this->surface = surface_khr;

    this->indices = Find_Queue_Families(this->physical_device, this->surface);

    this->transfer_and_graphics_queue_are_same = indices.transfer_whole_images_family == indices.graphics_family;

    Create_Command_Buffer_And_Pool(this->logical_device, this->command_pool_copy, this->command_buffer_copy, indices.transfer_whole_images_family);
    if (not this->transfer_and_graphics_queue_are_same) {
        Create_Command_Buffer_And_Pool(this->logical_device, this->command_pool_transfer_ownership, this->command_buffer_transfer_ownership, indices.graphics_family);
    }
    this->end_of_copy_fence = Create_New_VkFence(this->logical_device, false);

    vkGetDeviceQueue(this->logical_device, indices.transfer_whole_images_family, 0, &this->queue_copy);
    vkGetDeviceQueue(this->logical_device, indices.graphics_family, 0, &this->queue_graphics);

    VkSemaphoreCreateInfo semaphore_create_info {};
    semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    const VkResult semaphore_create_result { vkCreateSemaphore(this->logical_device, &semaphore_create_info, nullptr, &this->semaphore_synchronization_copy_and_transfer_ownership) };
    Throw_On_VkResult_Error("Could not create semaphore, cannot transfer textures to GPU memory", semaphore_create_result);
}

void Vulkan_Texture_Copy_Job_Scheduler::Add_Image_To_Transfer(std::span<const char> image_bytes, VkImage target_image, size_t width, size_t height, size_t depth, bool sharing_exclusive) {
    this->images_to_copy_information.emplace_back(image_bytes, target_image, width, height, depth, sharing_exclusive);
}

namespace {
void Copy_Textures_To_Host_Mapped_Memory(auto const& copy_information_array, std::span<const size_t> offsets_to_copy, void* host_mapped_memory_pointer_arg) {
    char* const host_mapped_memory_pointer { static_cast<char*>(host_mapped_memory_pointer_arg) };
    for (size_t i { 0 }; i < copy_information_array.size(); i++) {
        auto const& copy_information_current_texture { copy_information_array[i] };
        char* const pointer_copy_target_current_texture { host_mapped_memory_pointer + offsets_to_copy[i] };
        std::copy(copy_information_current_texture.image_bytes.begin(), copy_information_current_texture.image_bytes.end(),
                  pointer_copy_target_current_texture);
    }
}
}

void Vulkan_Texture_Copy_Job_Scheduler::Start_Copying_Images() {
    this->Create_Transfer_Source_Buffers_For_All_Images();
    const std::vector offsets_image_data { this->Get_Offsets_Inside_Buffer_For_All_Images() };
    const size_t full_size_data { offsets_image_data.back() + this->images_to_copy_information.back().image_bytes.size() };
    if (full_size_data > this->allocated_memory_size) {
        this->Reallocate_More_Memory(full_size_data);
    }

    Copy_Textures_To_Host_Mapped_Memory(this->images_to_copy_information, offsets_image_data, this->host_mapped_memory_pointer);

    VkCommandBufferBeginInfo begin_info {};
    begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    begin_info.pNext = nullptr;
    begin_info.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;
    begin_info.pInheritanceInfo = nullptr;
    VkResult begin_res { vkBeginCommandBuffer(this->command_buffer_copy, &begin_info) };
    Throw_On_VkResult_Error("Could not begin command buffer", begin_res);

    std::vector<VkImageMemoryBarrier> image_barriers {};
    for (size_t i { 0 }; i < this->images_to_copy_information.size(); i++) {
        VkImageMemoryBarrier image_barrier {};
        image_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
        image_barrier.pNext = nullptr;
        image_barrier.oldLayout = VK_IMAGE_LAYOUT_UNDEFINED;
        image_barrier.newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        image_barrier.srcAccessMask = 0;
        image_barrier.dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        image_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
        image_barrier.image = this->images_to_copy_information[i].target_image;
        image_barrier.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_barrier.subresourceRange.baseMipLevel = 0;
        image_barrier.subresourceRange.baseArrayLayer = 0;
        image_barrier.subresourceRange.levelCount = 1;
        image_barrier.subresourceRange.layerCount = 1;
        image_barriers.push_back(image_barrier);

        const VkResult res_bind_memory { vkBindBufferMemory(this->logical_device, this->buffers_source_pictures[i], this->memory_buffer_source, offsets_image_data[i]) };
        Throw_On_VkResult_Error("Could not bind host memory to the buffer for textures copying", res_bind_memory);
    }
    vkCmdPipelineBarrier(command_buffer_copy, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, image_barriers.size(), image_barriers.data());

    for (size_t i { 0 }; i < this->images_to_copy_information.size(); i++) {
        VkBufferImageCopy image_copy {};
        image_copy.bufferOffset = 0;
        image_copy.imageExtent.width = this->images_to_copy_information[i].width;
        image_copy.imageExtent.height = this->images_to_copy_information[i].height;
        image_copy.imageExtent.depth = 1;
        image_copy.bufferRowLength = 0;
        image_copy.bufferImageHeight = 0;
        image_copy.imageOffset.x = image_copy.imageOffset.y = image_copy.imageOffset.z = 0;
        image_copy.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        image_copy.imageSubresource.mipLevel = 0;
        image_copy.imageSubresource.baseArrayLayer = 0;
        image_copy.imageSubresource.layerCount = 1;

        image_barriers[i].oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        image_barriers[i].newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
        image_barriers[i].srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT;
        image_barriers[i].dstAccessMask = VK_ACCESS_SHADER_READ_BIT;
        if (not this->transfer_and_graphics_queue_are_same) {
            // Release the copy queue's ownership of the image.
            image_barriers[i].srcQueueFamilyIndex = this->indices.transfer_whole_images_family;
            image_barriers[i].dstQueueFamilyIndex = this->indices.graphics_family;
            image_barriers[i].dstAccessMask = 0; // NOT Ignored (when != 0) when releasing image ownership by at least the validation layers, the spec recommands setting it to 0 anyway.
        }

        vkCmdCopyBufferToImage(command_buffer_copy, this->buffers_source_pictures[i], this->images_to_copy_information[i].target_image,
                               VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &image_copy);
    }
    // Second barrier: this one transitions the layout into its final state, and possibly transfers ownerships to the graphics queue.
    vkCmdPipelineBarrier(command_buffer_copy, VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                         0, 0, nullptr, 0, nullptr, image_barriers.size(), image_barriers.data());
    VkResult end_res { vkEndCommandBuffer(command_buffer_copy) };
    Throw_On_VkResult_Error("Could not end command buffer", end_res);

    VkFence end_of_copy_fence_if_no_ownership_transfer { VK_NULL_HANDLE };

    std::vector<VkImageMemoryBarrier> acquire_ownership_barriers {};
    if (not this->transfer_and_graphics_queue_are_same) {
        for (size_t i { 0 }; i < this->images_to_copy_information.size(); i++) {
            if (this->images_to_copy_information[i].sharing_exclusive) {
                VkImageMemoryBarrier barrier { image_barriers[i] };
                // Per 7.7.4 in the spec: when combining ownership transfer and layout transition in a single barrier, the old and new layouts must match on both sides of the ownership transfer.
                barrier.oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;    // NOT a mistake (see 7.7.4: when combining ownership transfer and layout transition, the old and new layouts must match on both sides of the ownership transfer)
                barrier.newLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
                barrier.srcAccessMask = 0;
                barrier.dstAccessMask =  VK_ACCESS_SHADER_READ_BIT;
                barrier.srcQueueFamilyIndex = indices.transfer_whole_images_family;
                barrier.dstQueueFamilyIndex = indices.graphics_family;
                acquire_ownership_barriers.push_back(barrier);
            }
        }
        if (acquire_ownership_barriers.size() > 0) {
            // The graphics queue will acquire ownership of all exclusive VkImages.
            begin_res = vkBeginCommandBuffer(this->command_buffer_transfer_ownership, &begin_info);
            Throw_On_VkResult_Error("Could not begin command buffer", begin_res);

            vkCmdPipelineBarrier(this->command_buffer_transfer_ownership,
                                 VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_VERTEX_SHADER_BIT, //VK_PIPELINE_STAGE_VERTEX_INPUT_BIT, // VK_PIPELINE_STAGE_ALL_COMMANDS_BIT,
                                 0, 0, nullptr, 0, nullptr, acquire_ownership_barriers.size(), acquire_ownership_barriers.data());
 
            end_res = vkEndCommandBuffer(this->command_buffer_transfer_ownership);
            Throw_On_VkResult_Error("Could not end command buffer", end_res);
        }
        else {
            end_of_copy_fence_if_no_ownership_transfer = this->end_of_copy_fence;
        }
    }

    VkSubmitInfo submit_info {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer_copy;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &this->semaphore_synchronization_copy_and_transfer_ownership;

    VkResult submit_res { vkQueueSubmit(this->queue_copy, 1, &submit_info, end_of_copy_fence_if_no_ownership_transfer) };
    Throw_On_VkResult_Error("Texture copy to GPU: Failed to submit texture copy command buffer to queue", submit_res);

    if (acquire_ownership_barriers.size() > 0) {
        VkPipelineStageFlags wait_stage { VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT };
        VkSubmitInfo submit_info {};
        submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
        submit_info.waitSemaphoreCount = 1;
        // Because the ownership transfer command buffer will be waiting on that semaphore, it has to be submitted after the copy command buffer that will signal it.
        submit_info.pWaitSemaphores = &this->semaphore_synchronization_copy_and_transfer_ownership;
        submit_info.pWaitDstStageMask = &wait_stage;
        submit_info.commandBufferCount = 1;
        submit_info.pCommandBuffers = &this->command_buffer_transfer_ownership;

        const VkResult submit_res { vkQueueSubmit(this->queue_graphics, 1, &submit_info, this->end_of_copy_fence) };
        Throw_On_VkResult_Error("Texture copy to GPU: Failed to submit ownership transfer command buffer to queue", submit_res);
    }
}

void Vulkan_Texture_Copy_Job_Scheduler::Wait_For_Copying_To_End() {
    // Actually wait for the end of execution.
    VkResult wait_res { VK_TIMEOUT };
    while (wait_res == VK_TIMEOUT) {
        wait_res = vkWaitForFences(this->logical_device, 1, &this->end_of_copy_fence, false, 10000);
    }
    if (wait_res != VK_SUCCESS) {
        Throw_On_VkResult_Error("Texture to GPU copy: waiting for the end of copying fence failed", wait_res);
    }

    // Do some cleanup to prepare for a new batch of textures to copy.
    vkResetFences(this->logical_device, 1, &this->end_of_copy_fence);

    for (VkBuffer buffer: this->buffers_source_pictures) {
        vkDestroyBuffer(this->logical_device, buffer, nullptr);
    }
    this->buffers_source_pictures.clear();
    this->images_to_copy_information.clear();
}

void Vulkan_Texture_Copy_Job_Scheduler::Destroy() {
    vkDestroyFence(this->logical_device, this->end_of_copy_fence, nullptr);
    vkDestroySemaphore(this->logical_device, this->semaphore_synchronization_copy_and_transfer_ownership, nullptr);
    vkFreeCommandBuffers(this->logical_device, this->command_pool_copy, 1, &this->command_buffer_copy);
    vkFreeCommandBuffers(this->logical_device, this->command_pool_transfer_ownership, 1, &this->command_buffer_transfer_ownership);
    vkFreeMemory(this->logical_device, this->memory_buffer_source, nullptr);
    vkDestroyCommandPool(this->logical_device, this->command_pool_copy, nullptr);
    vkDestroyCommandPool(this->logical_device, this->command_pool_transfer_ownership, nullptr);
}

void Vulkan_Texture_Copy_Job_Scheduler::Create_Transfer_Source_Buffers_For_All_Images() {
    for (size_t i { 0 }; i < this->images_to_copy_information.size(); i++) {
        VkBufferCreateInfo buffer_cinfo {};
        buffer_cinfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_cinfo.flags = 0;
        buffer_cinfo.size = this->images_to_copy_information[i].image_bytes.size();
        buffer_cinfo.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
        buffer_cinfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
        buffer_cinfo.queueFamilyIndexCount = 0;
        buffer_cinfo.pQueueFamilyIndices = nullptr;

        VkBuffer buffer {};
        const VkResult res_buffer_create { vkCreateBuffer(this->logical_device, &buffer_cinfo, nullptr, &buffer) };
        Throw_On_VkResult_Error("Could not create buffer to copy to VkImage textures", res_buffer_create);
        this->buffers_source_pictures.push_back(buffer);
    }
}

std::vector<size_t> Vulkan_Texture_Copy_Job_Scheduler::Get_Offsets_Inside_Buffer_For_All_Images() const {
    std::vector<size_t> result {};
    size_t i { 0 }, total_size {};
    for (auto const& image_copy: this->images_to_copy_information) {
        VkMemoryRequirements buffer_requirements {};
        vkGetBufferMemoryRequirements(this->logical_device, this->buffers_source_pictures[i], &buffer_requirements);
        result.push_back(math::Get_Next_Aligned_Offset(total_size, buffer_requirements.alignment));
        total_size += image_copy.image_bytes.size();
        i++;
    }
    return result;
}

namespace {
std::optional<uint32_t> Get_Host_Memory_Type(VkPhysicalDevice physical_device) {
    VkPhysicalDeviceMemoryProperties2 mem_props2 {};
    mem_props2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
    vkGetPhysicalDeviceMemoryProperties2(physical_device, &mem_props2);
    for (unsigned int i { 0 }; i < mem_props2.memoryProperties.memoryTypeCount; i++) {
        const auto type = mem_props2.memoryProperties.memoryTypes[i];
        if (type.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
            return i;
        }
    }
    return std::nullopt;
}
}

void Vulkan_Texture_Copy_Job_Scheduler::Reallocate_More_Memory(size_t size_new_block) {
    // Forget the previous block (if any).
    vkFreeMemory(this->logical_device, this->memory_buffer_source, nullptr);

    const auto host_mem_index { Get_Host_Memory_Type(this->physical_device) };
    if (not host_mem_index.has_value()) {
        throw Vulkan_API_Call_Error { "Could not find host visible memory." };
    }

    VkMemoryAllocateInfo allocate_info {};
    allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocate_info.pNext = nullptr;
    allocate_info.allocationSize = size_new_block;
    allocate_info.memoryTypeIndex = *host_mem_index;

    const VkResult res_mem_allocate { vkAllocateMemory(this->logical_device, &allocate_info, nullptr, &this->memory_buffer_source) };
    Throw_On_VkResult_Error("Could not allocate host memory to copy textures.", res_mem_allocate);

    const VkResult res_map { vkMapMemory(this->logical_device, this->memory_buffer_source, 0, size_new_block, 0, &this->host_mapped_memory_pointer) };
    Throw_On_VkResult_Error("Could not map host visible memory", res_map);

    this->allocated_memory_size = size_new_block;
}
