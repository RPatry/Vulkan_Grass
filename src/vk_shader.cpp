#include "vk_shader.hpp"
#include <iostream>
#include <format>
#include "vk_exceptions.hpp"

std::ostream& operator<<(std::ostream& os, VkResult res);

std::optional<VkShaderModule> Create_Shader_Module(VkDevice device, unsigned int const* const bytecode, size_t size_shader) {
    VkShaderModuleCreateInfo cinfo = {};
    cinfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    cinfo.codeSize = size_shader;
    
    cinfo.pCode = bytecode;
    VkShaderModule shader_module {};
    if (VkResult res { vkCreateShaderModule(device, &cinfo, nullptr, &shader_module) };
        res != VK_SUCCESS) {
        std::cerr << "Warning: shader module not created, result: " << res << std::endl;
        return std::nullopt;
    }
    return shader_module;
}

namespace {
VkShaderModule Try_Load_Shader_Module(VkDevice logical_device, Shader_SPIRV_View spirv_bytecode, char const* pipeline_name, char const* shader_name) {
    std::optional<VkShaderModule> maybe_module { Create_Shader_Module(logical_device, spirv_bytecode.data(), spirv_bytecode.size()) };
    if (not maybe_module.has_value()) {
        const std::string error_message { std::format("{} {} shader not loaded", pipeline_name, shader_name) };
        throw Vulkan_API_Call_Error { error_message };
    }
    return *maybe_module;
}
}

Basic_Pipeline_Shaders_Set Create_Shader_Pipeline_Set(VkDevice logical_device, Shader_SPIRV_View vertex_spirv, Shader_SPIRV_View fragment_spirv, char const* pipeline_name) {
    VkShaderModule vertex_shader { Try_Load_Shader_Module(logical_device, vertex_spirv, pipeline_name, "vertex") },
                   fragment_shader { Try_Load_Shader_Module(logical_device, fragment_spirv, pipeline_name, "fragment") };
    return Basic_Pipeline_Shaders_Set { vertex_shader, fragment_shader, logical_device };
}

Pipeline_Shaders_Set_With_Tessellation Create_Shader_Pipeline_Set(VkDevice logical_device, Shader_SPIRV_View vertex_spirv, Shader_SPIRV_View tessellation_control_spirv,
                                                                  Shader_SPIRV_View tessellation_evaluation_spirv, Shader_SPIRV_View fragment_spirv, char const* pipeline_name) {
    VkShaderModule vertex_shader { Try_Load_Shader_Module(logical_device, vertex_spirv, pipeline_name, "vertex") },
                   tessellation_control_shader { Try_Load_Shader_Module(logical_device, tessellation_control_spirv, pipeline_name, "tessellation control") },
                   tessellation_evaluation_shader { Try_Load_Shader_Module(logical_device, tessellation_evaluation_spirv, pipeline_name, "tessellation evaluation") },
                   fragment_shader { Try_Load_Shader_Module(logical_device, fragment_spirv, pipeline_name, "fragment") };
    return Pipeline_Shaders_Set_With_Tessellation { tessellation_control_shader, tessellation_evaluation_shader, vertex_shader, fragment_shader, logical_device };
}
