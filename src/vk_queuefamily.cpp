#include "vk_queuefamily.hpp"
#include <bit>
#include <ranges>
#include <algorithm>

namespace {
int Find_Dedicated_Whole_Image_Transfer_Queue_Family(auto const& queue_families) {
    const auto Compare_Queue_Features_Count = [](auto const& fam1, auto const& fam2) {
        // The VK_QUEUE_*_BIT are powers of two: thus, the queue that has the least bits set supports the fewest features (and is the closest to a dedicated transfer queue).
        return std::popcount(std::get<1>(fam1).queueFlags) < std::popcount(std::get<1>(fam2).queueFlags);
    };
    // Any queue supporting transfer operations supports copying entire images, regardless of the granularity reported in minImageTransferGranularity,
    // per https://registry.khronos.org/vulkan/specs/1.3-extensions/html/vkspec.html#VkQueueFamilyProperties. So no need to do any filtering there.
    const auto queue_family_supports_transfer = [](auto const& fam) { return std::get<1>(fam).queueFlags & VK_QUEUE_TRANSFER_BIT; };
    auto view_transfer_queues = std::views::enumerate(queue_families) |
                                std::views::filter(queue_family_supports_transfer);
    return std::get<0>(*std::ranges::min_element(view_transfer_queues, Compare_Queue_Features_Count));
}
}

Queue_Family_Indices Find_Queue_Families(VkPhysicalDevice device, VkSurfaceKHR surface) {
    Queue_Family_Indices indices {};
    const std::vector queue_families { Find_All_Queue_Families_Properties(device) };

    indices.transfer_whole_images_family = Find_Dedicated_Whole_Image_Transfer_Queue_Family(queue_families);

    for (auto const& [i, fam]: std::views::enumerate(queue_families)) {
        if ((fam.queueCount > 0) and (fam.queueFlags & VK_QUEUE_GRAPHICS_BIT)) {
            indices.graphics_family = i;
        }
        if ((fam.queueCount > 0) and (fam.queueFlags & VK_QUEUE_COMPUTE_BIT)) {
            indices.compute_family = i;
        }
        if ((fam.queueCount > 0) and (fam.queueFlags & VK_QUEUE_TRANSFER_BIT)) {
            indices.transfer_family = i;
        }
        VkBool32 supports_presentation {};
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, surface, &supports_presentation);
        if (fam.queueCount > 0 and supports_presentation) {
            indices.present_family = i;
        }
        if (indices.Is_Complete()) {
            break;
        }
    }
    return indices;
}

std::vector<VkQueueFamilyProperties> Find_All_Queue_Families_Properties(VkPhysicalDevice device) {
    uint32_t nb_fams {};
    vkGetPhysicalDeviceQueueFamilyProperties(device, &nb_fams, nullptr);

    std::vector<VkQueueFamilyProperties> families { nb_fams };
    vkGetPhysicalDeviceQueueFamilyProperties(device, &nb_fams, families.data());

    return families;
}

int Find_Queue_Family_For_Transfer(VkPhysicalDevice device) {
    const auto queue_families = Find_All_Queue_Families_Properties(device);
    for (int i { 0 }; auto const& fam: queue_families) {
        if (fam.queueCount > 0 and (fam.queueFlags & VK_QUEUE_TRANSFER_BIT)) {
            return i;
        }
        i++;
    }
    return -1;
}
