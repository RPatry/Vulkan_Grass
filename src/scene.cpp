#include "scene.hpp"
#include <chrono>
#include <vector>
#include <random>
#include <span>
#include <numbers>
#include <algorithm>
#include <ranges>
#include <iostream>
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include "utils.hpp"
#include "math.hpp"

namespace scene {
std::random_device random_device;
std::mt19937 mersenne_generator { random_device() };
}

namespace scene::static_patch {
std::vector<Grass_Blade_Vertex> grass_blade_vertices {};

constexpr int row_blade_count { 20 };
constexpr int blade_count { 400 };
constexpr int row_count { static_cast<int>(math::ceil(static_cast<double>(blade_count) / row_blade_count)) };
constexpr double blade_spacing { 25 };
constexpr double patch_width { row_blade_count * blade_spacing };
constexpr double patch_depth { row_blade_count * blade_spacing };

constexpr double ground_y { 0 };
constexpr double ground_max_x { patch_width };
constexpr double texture_max_x { row_blade_count };
constexpr double ground_max_z { patch_depth };
constexpr double texture_max_z { row_count };
std::vector<Grass_Ground_Vertex> ground_vertices {
    // First triangle.
    { glm::vec3 { 0, ground_y, 0 }, glm::vec2 { 0, texture_max_z } },
    { glm::vec3 { ground_max_x, ground_y, 0 }, glm::vec2 { texture_max_x, texture_max_z } },
    { glm::vec3 { ground_max_x, ground_y, ground_max_z }, glm::vec2 { texture_max_x, 0 } },
    // Second triangle.
    { glm::vec3 { ground_max_x, ground_y, ground_max_z }, glm::vec2 { texture_max_x, 0 } },
    { glm::vec3 { 0, ground_y, ground_max_z }, glm::vec2 { 0, 0 } },
    { glm::vec3 { 0, ground_y, 0 }, glm::vec2 { 0, texture_max_z } },
};

struct Static_Grass_Blade {
    glm::vec3 position {};
    int index {};
    double height {};
    static constexpr double width { 2.5 };
};

glm::vec3 Create_Random_Blade_Color() {
    std::normal_distribution<double> green_distribution { 192 / 255.0, 0.2 };
    std::uniform_real_distribution<double> red_distribution { 0.0, 25 / 255.0 };
    std::uniform_real_distribution<double> blue_distribution { 64 / 255.0, 90 / 255.0 };

    const double red { red_distribution(mersenne_generator) };
    const double green { std::clamp(green_distribution(mersenne_generator), 0.0, 1.0) };
    const double blue { blue_distribution(mersenne_generator) };
    return glm::vec3 { red, green, blue };
}

void Create_Static_Patch() {
    std::normal_distribution<double> blade_height_distribution { 25, 2.8 };
    std::normal_distribution<double> blade_x_deviation { 0, 2 };
    std::normal_distribution<double> blade_z_deviation { 0, 1 };
    std::vector<Static_Grass_Blade> patch;

    for (int i { 0 }; i < blade_count; i++) {
        const double x { ((i % row_blade_count) * blade_spacing) + blade_x_deviation(mersenne_generator) };
        const double z { ((i / row_blade_count) * blade_spacing) + blade_z_deviation(mersenne_generator) };
        Static_Grass_Blade new_blade {
            .position = glm::vec3(x, 0, z),
            .index = i,
            .height = blade_height_distribution(mersenne_generator)
        };
        patch.push_back(new_blade);
    }
    for (auto const& blade: patch) {
        glm::vec3 bottom_vertex_1 {}, bottom_vertex_2 {}, top_vertex {};
        bottom_vertex_1.x = blade.position.x - Static_Grass_Blade::width / 2;
        bottom_vertex_2.x = blade.position.x + Static_Grass_Blade::width / 2;
        bottom_vertex_1.y = bottom_vertex_2.y = blade.position.y;
        bottom_vertex_1.z = bottom_vertex_2.z = top_vertex.z = blade.position.z;
        top_vertex.x = blade.position.x;
        top_vertex.y = blade.position.y + blade.height;
        const auto color { Create_Random_Blade_Color() };
        // Create two triangles, for front and back
        for (glm::vec3 vertex: { bottom_vertex_1, bottom_vertex_2, top_vertex,
                                 bottom_vertex_2, bottom_vertex_1, top_vertex }) {
            grass_blade_vertices.push_back(Grass_Blade_Vertex { vertex, color });
        }
    }
}

Image_File_Data static_grass_ground_texture_data {};
}

namespace scene {

double Time_Spent { 0.0 };
double Duration_Last_Frame { 0.0 };
auto time_point_program_start { std::chrono::high_resolution_clock::now() };
auto last_frame_time_point { std::chrono::high_resolution_clock::now() };

void Update_Time_Spent() {
    const auto now { std::chrono::high_resolution_clock::now() };
    const auto duration_since_program_start { now - time_point_program_start };
    const auto duration_since_last_frame { now - last_frame_time_point };
    Time_Spent = std::chrono::duration_cast<std::chrono::microseconds>(duration_since_program_start).count();
    Duration_Last_Frame = std::chrono::duration_cast<std::chrono::microseconds>(duration_since_last_frame).count();
    last_frame_time_point = now;
}

constexpr glm::vec3 initial_camera_position { 5, 15, -50 };
glm::vec3 camera_position { initial_camera_position };
glm::vec2 camera_angle { 0, 0 };
//! The speed per microsecond to move the camera around at.
const double camera_speed { 0.00025 };
//! The speed per microsecond to rotate the camera around.
const double camera_angular_speed { std::numbers::pi / 32 / 10000 };

constexpr double Camera_Near { 0.1 }, Camera_Far { 8000 };
glm::mat4 perspective_projection_matrix { glm::perspective(glm::radians(90.0), 1.0, Camera_Near, Camera_Far) };
glm::mat4 projection_matrix;
const glm::vec3 center_camera { 0, 5, 50 };
glm::mat4 view_matrix { glm::lookAt(camera_position, center_camera, glm::vec3 { 0.0, 1.0, 0.0 }) };

extern "C" {
    extern const char Height_Map_Test [];
    extern const unsigned int Height_Map_Test_Size;
    extern const char Height_Map_2 [];
    extern const unsigned int Height_Map_2_Size;
}

Height_Map_Data test_height_map_data { Read_Height_Map_From_PNG(std::span<const char> { Height_Map_Test, Height_Map_Test_Size }, true) };
Height_Map_Data bigger_height_map_data { Read_Height_Map_From_PNG(std::span<const char> { Height_Map_2, Height_Map_2_Size }, true) };

void Update_Camera_Matrix() {
    glm::vec3 local_center_camera = glm::rotate(scene::center_camera, camera_angle.y, glm::vec3 { 1.0, 0.0, 0.0 });
    local_center_camera = glm::rotate(local_center_camera, camera_angle.x, glm::vec3 { 0.0, 1.0, 0.0 });
    local_center_camera += camera_position;
    view_matrix = glm::lookAt(camera_position, local_center_camera, glm::vec3 { 0.0, 1.0, 0.0 });
    projection_matrix = perspective_projection_matrix * view_matrix;
}

void Advance_Camera() {
    camera_position.z += camera_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Move_Camera_Backwards() {
    camera_position.z -= camera_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Advance_Camera_Left() {
    camera_position.x -= camera_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Advance_Camera_Right() {
    camera_position.x += camera_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Move_Camera_Left() {
    camera_angle.x -= camera_angular_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Move_Camera_Right() {
    camera_angle.x += camera_angular_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Move_Camera_Up() {
    camera_angle.y -= camera_angular_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Move_Camera_Down() {
    camera_angle.y += camera_angular_speed * Duration_Last_Frame;
    Update_Camera_Matrix();
}

void Reset_Camera() {
    camera_angle = glm::vec2 { 0.0, 0.0 };
    camera_position = initial_camera_position;
    Update_Camera_Matrix();
}

int window_width {}, window_height {};
void Set_Window_Size(int width, int height) {
    width = std::max(width, 1);
    height = std::max(height, 1);
    window_width = width;
    window_height = height;
    perspective_projection_matrix = glm::perspective(glm::radians(90.0), static_cast<double>(width) / height, Camera_Near, Camera_Far);
    perspective_projection_matrix[1][1] *= -1; // Invert Y axis
    perspective_projection_matrix[0][0] *= -1; // Invert X axis
    Update_Camera_Matrix();
}

extern "C" {
    extern const char Grass_Ground_Texture [];
    extern const unsigned int Grass_Ground_Texture_Size;
}

namespace {
std::vector<Image_File_Data> height_maps_textures {};
std::vector<std::array<glm::vec3, 4>> height_maps_patches {};

//! Create a tessellation patch for a height map terrain whose up vector aligns with the Y ascending vector of the scene.
std::array<glm::vec3, 4> Make_Height_Map_Patch(double x_left, double z_top, double y, double width, double height) {
    return { glm::vec3 { x_left, y, z_top }, glm::vec3 { x_left + width, y, z_top },
             glm::vec3 { x_left, y, z_top + height}, glm::vec3 { x_left + width, y, z_top + height} };
}
}

void Load_Textures() {
    static_patch::static_grass_ground_texture_data = Read_PNG_File_From_Memory(std::span<const char> { Grass_Ground_Texture, Grass_Ground_Texture_Size });
    height_maps_textures.push_back(Read_PNG_File_From_Memory(std::span<const char> { Height_Map_Test, Height_Map_Test_Size }));
    height_maps_textures.push_back(Read_PNG_File_From_Memory(std::span<const char> { Height_Map_2, Height_Map_2_Size }));
}

void Create_Patches() {
    height_maps_patches.push_back(Make_Height_Map_Patch(50, -100, -25, 1000, -1000));
    height_maps_patches.push_back(Make_Height_Map_Patch(-2050, -100, -25, 2048, -2048));
}

std::pair<unsigned int, unsigned int> Get_Texture_Size(int index) {
    if (index == 0) {
        return { static_patch::static_grass_ground_texture_data.width, static_patch::static_grass_ground_texture_data.height };
    }
    return { 0, 0 };
}

unsigned int Get_Total_Size_Vertex_Data() {
    return Vector_Byte_Size(static_patch::grass_blade_vertices) + Vector_Byte_Size(static_patch::ground_vertices);
}

unsigned int Get_Total_Size_Textures() {
    return static_patch::static_grass_ground_texture_data.pixels.size();
}

std::pair<unsigned int, unsigned int> Get_Texture_Size() {
    return std::pair { static_patch::static_grass_ground_texture_data.width, static_patch::static_grass_ground_texture_data.height };
}

void Copy_Texture_Data(char* pointer_host_memory) {
    std::ranges::copy(static_patch::static_grass_ground_texture_data.pixels, pointer_host_memory);
}

std::span<const char> Get_Ground_Texture_Data() {
    return static_patch::static_grass_ground_texture_data.pixels;
}

std::vector<std::span<const char>> Get_Height_Maps_Texture_Pixels() {
    std::vector<std::span<const char>> pixels {};
    for (auto const& height_map: height_maps_textures) {
        pixels.push_back(height_map.pixels);
    }
    return pixels;
}

std::vector<std::pair<unsigned int, unsigned int>> Get_Height_Maps_Textures_Sizes() {
    std::vector<std::pair<unsigned int, unsigned int>> sizes {};
    for (auto const& hm: height_maps_textures) {
        sizes.emplace_back(hm.width, hm.height);
    }
    return sizes;
}

std::vector<std::span<const glm::vec3>> Get_Height_Map_Tessellation_Patches() {
    std::vector<std::span<const glm::vec3>> patches {};
    for (auto const& patch: height_maps_patches) {
        patches.push_back(patch);
    }
    return patches;
}

std::vector<std::span<const uint8_t>> Get_Height_Maps() {
    return { test_height_map_data.heights, bigger_height_map_data.heights };
}
}
