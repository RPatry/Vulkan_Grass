#include "vk.hpp"
#include "vk_validation.hpp"
#include "vk_extensions.hpp"
#include "vk_queuefamily.hpp"
#include "vk_swapchain.hpp"
#include "vk_shader.hpp"
#include "vk_texture_copy.hpp"
#include "vk_image_helpers.hpp"
#include "utils.hpp"
#include "shaders.hpp"
#include "scene.hpp"
#include <iostream>
#include <algorithm>
#include <limits>
#include <set>
#include <map>
#include <string>
#include <functional>
#include <bit>
#include <ranges>
#include <format>

using namespace std::placeholders;

std::ostream& operator<<(std::ostream& os, VkResult res) {
    static const std::map<VkResult, char const*> map_error_code_to_string_representation {
        {VK_SUCCESS, "VK_SUCCESS"},
        {VK_NOT_READY, "VK_NOT_READY"},
        {VK_TIMEOUT, "VK_TIMEOUT"},
        {VK_EVENT_SET, "VK_EVENT_SET"},
        {VK_EVENT_RESET, "VK_EVENT_RESET"},
        {VK_INCOMPLETE, "VK_INCOMPLETE"},
        {VK_ERROR_OUT_OF_HOST_MEMORY , "VK_ERROR_OUT_OF_HOST_MEMORY"},
        {VK_ERROR_OUT_OF_DEVICE_MEMORY , "VK_ERROR_OUT_OF_DEVICE_MEMORY"},
        {VK_ERROR_INITIALIZATION_FAILED , "VK_ERROR_INITIALIZATION_FAILED"},
        {VK_ERROR_DEVICE_LOST , "VK_ERROR_DEVICE_LOST"},
        {VK_ERROR_MEMORY_MAP_FAILED , "VK_ERROR_MEMORY_MAP_FAILED"},
        {VK_ERROR_LAYER_NOT_PRESENT , "VK_ERROR_LAYER_NOT_PRESENT"},
        {VK_ERROR_EXTENSION_NOT_PRESENT , "VK_ERROR_EXTENSION_NOT_PRESENT"},
        {VK_ERROR_FEATURE_NOT_PRESENT , "VK_ERROR_FEATURE_NOT_PRESENT"},
        {VK_ERROR_INCOMPATIBLE_DRIVER , "VK_ERROR_INCOMPATIBLE_DRIVER"},
        {VK_ERROR_TOO_MANY_OBJECTS , "VK_ERROR_TOO_MANY_OBJECTS"},
        {VK_ERROR_FORMAT_NOT_SUPPORTED , "VK_ERROR_FORMAT_NOT_SUPPORTED"},
        {VK_ERROR_FRAGMENTED_POOL , "VK_ERROR_FRAGMENTED_POOL"},
        {VK_ERROR_UNKNOWN, "VK_ERROR_UNKNOWN"},
        {VK_ERROR_OUT_OF_POOL_MEMORY, "VK_ERROR_OUT_OF_POOL_MEMORY"},
        {VK_ERROR_INVALID_EXTERNAL_HANDLE, "VK_ERROR_INVALID_EXTERNAL_HANDLE"},
        {VK_ERROR_FRAGMENTATION, "VK_ERROR_FRAGMENTATION"},
        {VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS, "VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS"},
        {VK_ERROR_SURFACE_LOST_KHR, "VK_ERROR_SURFACE_LOST_KHR"},
        {VK_ERROR_NATIVE_WINDOW_IN_USE_KHR, "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR"},
        {VK_SUBOPTIMAL_KHR, "VK_SUBOPTIMAL_KHR"},
        {VK_ERROR_OUT_OF_DATE_KHR, "VK_ERROR_OUT_OF_DATE_KHR"},
        {VK_ERROR_INCOMPATIBLE_DISPLAY_KHR, "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR"},
        {VK_ERROR_VALIDATION_FAILED_EXT, "VK_ERROR_VALIDATION_FAILED_EXT"},
        {VK_ERROR_INVALID_SHADER_NV, "VK_ERROR_INVALID_SHADER_NV"},
    };
    if (map_error_code_to_string_representation.contains(res)) {
        return os << map_error_code_to_string_representation.at(res);
    }
    else {
        return os << "Unknown VkResult value (" << static_cast<unsigned int>(res) << ')';
    }
}

VkBool32 Debug_Callback_Straightforward
(
    VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
    VkDebugUtilsMessageTypeFlagsEXT messageTypes,
    VkDebugUtilsMessengerCallbackDataEXT const* pCallbackData,
    [[maybe_unused]] void* pUserData) {
    switch (messageSeverity) {
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT: {
            std::cerr << "Debug callback: Information - ";
            break;
        }
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT: {
            std::cerr << "Debug callback: Warning - ";
            break;
        }
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT: {
            std::cerr << "Debug callback: Error - ";
            break;
        }
        case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT:
        default: {
            return VK_FALSE;
        }
    }
    switch (messageTypes) {
        case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT: {
            std::cerr << " Validation - ";
            break;
        }
        case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT: {
            std::cerr << " Performance - ";
            break;
        }
        default: break;
    }
    std::cerr << '[' << pCallbackData->pMessageIdName << "] " << pCallbackData->pMessage << std::endl;
    // An application-defined PFN_vkDebugUtilsMessengerCallbackEXT must always return false.
    return VK_FALSE;
}

void Vk_Buffer_Info::Bind_Buffer_Memory(VkDevice logical_device) {
    const auto res_bind_buffer_memory { vkBindBufferMemory(logical_device, this->buffer, this->memory_slice->Get_VkDeviceMemory(), this->memory_slice->Get_Offset()) };
    Throw_On_VkResult_Error("Could not bind buffer memory", res_bind_buffer_memory);
}

void Vk_Image_Info::Bind_Image_Memory(VkDevice logical_device) {
    const auto res_bind_image_memory { vkBindImageMemory(logical_device, this->image, this->memory_slice->Get_VkDeviceMemory(), this->memory_slice->Get_Offset()) };
    Throw_On_VkResult_Error("Could not bind image memory", res_bind_image_memory);
}

void Vk_Image_Info::Destroy(VkDevice logical_device) {
    vkDestroyImageView(logical_device, this->image_view, nullptr);
    vkDestroyImage(logical_device, this->image, nullptr);
}

VkApplicationInfo Get_Application_Info(char const* app_name) noexcept {
    if (app_name == nullptr) {
        app_name = "Default application name";
    }
    VkApplicationInfo info {};
    info.pApplicationName = app_name;
    info.apiVersion = VK_API_VERSION_1_2;
    info.pEngineName = "Herba engine";
    info.applicationVersion = info.engineVersion = 1;
    info.pNext = nullptr;
    info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    return info;
}

void Vk_State::Verify_Vk_API_Version_Suitable() {
    uint32_t api_version {};
    vkEnumerateInstanceVersion(&api_version);
    constexpr int minor_version_required { 2 };
    if ((VK_API_VERSION_MAJOR(api_version) == 1) and (VK_API_VERSION_MINOR(api_version) < minor_version_required)) {
        throw Vulkan_API_Call_Error { std::format("This application requires Vulkan version >= 1.{}", minor_version_required) };
    }
}

Vk_State::~Vk_State() {
    vkDestroyBuffer(this->logical_device, this->static_patch.blades_vertex_buffer.buffer, nullptr);
    vkDestroyBuffer(this->logical_device, this->static_patch.ground_vertex_buffer.buffer, nullptr);
    vkDestroyBuffer(this->logical_device, this->matrix_buffer.buffer, nullptr);
    vkDestroyBuffer(this->logical_device, this->height_maps_patches_vertex_buffer.buffer, nullptr);

    for (auto& image_info: this->height_maps_textures_images_info) {
        image_info.Destroy(this->logical_device);
    }
    for (auto& image_info: this->height_maps_images_info) {
        image_info.Destroy(this->logical_device);
    }
    this->static_patch.ground_grass_texture.Destroy(this->logical_device);

    vkDestroySampler(this->logical_device, this->sampler_textures, nullptr);
    vkDestroySampler(this->logical_device, this->sampler_height_maps, nullptr);

    vkDestroySemaphore(this->logical_device, this->semaphore_render_finished, nullptr);
    vkDestroySemaphore(this->logical_device, this->semaphore_image_available, nullptr);

    this->Clean_Up_Swap_Chain();

    vkDestroyCommandPool(this->logical_device, this->graphics_command_pool, nullptr);
    if (enableValidationLayers) {
        auto vkDestroyDebugUtilsMessenger = reinterpret_cast<PFN_vkDestroyDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(this->instance, "vkDestroyDebugUtilsMessengerEXT"));
        if (vkDestroyDebugUtilsMessenger != nullptr) {
            vkDestroyDebugUtilsMessenger(this->instance, this->callback, nullptr);
        }
    }
    this->depth_buffer.Clean_Resources();

    this->memory_layout.Deallocate();

    vkDestroyDevice(this->logical_device, nullptr);
    vkDestroySurfaceKHR(this->instance, this->surface, nullptr);
    vkDestroyInstance(this->instance, nullptr);
}

void Vk_State::Create_Vk_Instance() {
    const auto app_info { Get_Application_Info("Vulkan Grass") };
    VkInstanceCreateInfo cinfo {};
    cinfo.enabledExtensionCount = this->extensions.size();
    cinfo.ppEnabledExtensionNames = this->extensions.data();
    if (enableValidationLayers) {
        cinfo.enabledLayerCount = validationLayers.size();
        cinfo.ppEnabledLayerNames = validationLayers.data();
    }
    cinfo.pApplicationInfo = &app_info;
    cinfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    auto result = vkCreateInstance(&cinfo, nullptr, &(this->instance));
    if (result != VkResult::VK_SUCCESS) {
        throw Vulkan_API_Call_Error { "vkCreateInstance failed: ", result };
    }
}

void Vk_State::Initialize_State() {
    if (enableValidationLayers and !Check_Layer_Validation_Support()) {
        throw Vulkan_API_Call_Error { "Validation layer failure" };
    }
    Get_All_Required_Instance_Extensions(this->window);

    this->Verify_Vk_API_Version_Suitable();
    this->Create_Vk_Instance();

    if (enableValidationLayers) {
        this->Setup_Debug_Callback();
    }
    if (not SDL_Vulkan_CreateSurface(this->window, this->instance, &this->surface)) {
        throw Vulkan_API_Call_Error { "Impossible to create surface!" };
    }
    this->Select_Physical_Device();
    this->Setup_Logical_Device();
    this->Setup_Samplers();
    this->Setup_Vertex_Buffers();
    this->Setup_Matrix_Buffer();
    this->Setup_Texture_Images();
    this->Setup_Depth_Buffer();
    this->Allocate_Device_Memory();
    this->Setup_Texture_Image_Views();
    this->Setup_Swap_Chain();
    this->Setup_Swap_Chain_Image_Views();
    this->Setup_Render_Pass();
    this->Setup_Descriptor_Pools();
    this->Setup_Graphics_Pipelines();
    this->Setup_Framebuffers();
    this->Setup_Graphics_Command_Pool();
    this->Setup_Command_Buffers();
    this->Setup_Semaphores();
    this->Copy_Textures_To_Images();
}

VkResult Create_Debug_Report_Callback_EXT(VkInstance instance, VkDebugUtilsMessengerCreateInfoEXT const& create_info, VkAllocationCallbacks const* pAllocator,
                                          VkDebugUtilsMessengerEXT* pCallback) {
    auto vkCreateDebugUtilsMessenger = reinterpret_cast<PFN_vkCreateDebugUtilsMessengerEXT>(vkGetInstanceProcAddr(instance, "vkCreateDebugUtilsMessengerEXT"));
    if (vkCreateDebugUtilsMessenger != nullptr) {
        return vkCreateDebugUtilsMessenger(instance, &create_info, pAllocator, pCallback);
    } else {
        throw Vulkan_API_Call_Error { "Could not retrieve vkCreateDebugUtilsMessengerEXT function" };
    }
}

void Vk_State::Setup_Debug_Callback() {
    VkDebugUtilsMessengerCreateInfoEXT debug_utils_messenger_create_info {};
    debug_utils_messenger_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debug_utils_messenger_create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debug_utils_messenger_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debug_utils_messenger_create_info.pfnUserCallback = &Debug_Callback_Straightforward;
    Create_Debug_Report_Callback_EXT(this->instance, debug_utils_messenger_create_info, nullptr, &this->callback);
}

void Vk_State::Get_All_Required_Instance_Extensions(SDL_Window* window) {
    this->extensions.clear();
    if (window != nullptr) {
        unsigned int count {};
        if (SDL_Vulkan_GetInstanceExtensions(window, &count, nullptr) == SDL_TRUE) {
            this->extensions.resize(count);
            SDL_Vulkan_GetInstanceExtensions(window, &count, this->extensions.data());
        }
    }
    this->extensions.push_back("VK_KHR_get_physical_device_properties2");
    if (enableValidationLayers) {
        this->extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
    }
}

bool Is_Device_Suitable(VkPhysicalDevice device, VkSurfaceKHR surface) {
    VkPhysicalDeviceProperties dev_props {};
    vkGetPhysicalDeviceProperties(device, &dev_props);
    VkPhysicalDeviceFeatures device_features {};
    vkGetPhysicalDeviceFeatures(device, &device_features);
    const auto indices = Find_Queue_Families(device, surface);
    const bool has_extensions { Check_Device_Has_All_Extensions(device) };
    const bool has_tessellation { device_features.tessellationShader == true };
    bool swap_chain_ok { false };
    if (has_extensions) {
        const auto support_details { Get_Swap_Chain_Support_Details(device, surface) };
        swap_chain_ok = not (support_details.formats.empty() or support_details.present_modes.empty());
    }
    return indices.Is_Complete() and has_extensions and swap_chain_ok and has_tessellation;
}

void Vk_State::Select_Physical_Device() {
    unsigned int nb_devices {};
    vkEnumeratePhysicalDevices(instance, &nb_devices, nullptr);
    if (nb_devices == 0) {
        throw Vulkan_API_Call_Error { "No VkPhysicalDevice could be retrieved." };
    }
    std::vector<VkPhysicalDevice> all_devices { nb_devices };
    vkEnumeratePhysicalDevices(this->instance, &nb_devices, all_devices.data());
    const auto is_device_suitable { std::bind(Is_Device_Suitable, _1, this->surface) };
    const auto iter { std::ranges::find_if(all_devices, is_device_suitable) };
    if (iter == all_devices.end()) {
        throw Vulkan_API_Call_Error { "No suitable VkPhysicalDevice found." };
    }
    this->physical_device = *iter;
}

bool Check_Sampler2D_Non_Uniform_Index_Available(VkPhysicalDevice physical_device) {
    VkPhysicalDeviceFeatures2 features2 {};
    features2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    VkPhysicalDeviceVulkan12Features features12 {};
    features12.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    features12.pNext = nullptr;
    features2.pNext = &features12;
    vkGetPhysicalDeviceFeatures2(physical_device, &features2);
    return features12.shaderSampledImageArrayNonUniformIndexing;
}

void Vk_State::Setup_Logical_Device() {
    const auto indices { Find_Queue_Families(this->physical_device, this->surface) };

    std::vector<VkDeviceQueueCreateInfo> queues_cinfos {};
    const std::set<int> families { indices.Get_All_Unique_Families() };
    const float priority { 1.0f };

    for (int fam: families) {
        VkDeviceQueueCreateInfo queue_create_info {};
        queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queue_create_info.queueFamilyIndex = fam;
        queue_create_info.queueCount = 1;

        queue_create_info.pQueuePriorities = &priority;
        queues_cinfos.push_back(queue_create_info);
    }

    VkPhysicalDeviceFeatures wanted_features {};
    wanted_features.depthBounds = true;
    wanted_features.tessellationShader = true;

    VkPhysicalDeviceVulkan12Features wanted_features_12 {};
    wanted_features_12.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_2_FEATURES;
    wanted_features_12.descriptorIndexing = true;
    wanted_features_12.separateDepthStencilLayouts = true;
    if (Check_Sampler2D_Non_Uniform_Index_Available(this->physical_device)) {
        wanted_features_12.shaderSampledImageArrayNonUniformIndexing = true;
    }

    VkPhysicalDeviceVulkan11Features wanted_features_11 {};   // No specific 1.1 features are needed, for now.
    wanted_features_11.sType =  VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_VULKAN_1_1_FEATURES;
    wanted_features_11.pNext = &wanted_features_12;

    VkDeviceCreateInfo cinfo {};
    cinfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    cinfo.pNext = &wanted_features_11;
    cinfo.pQueueCreateInfos = queues_cinfos.data();
    cinfo.queueCreateInfoCount = static_cast<uint32_t>(queues_cinfos.size());
    cinfo.pEnabledFeatures = &wanted_features;
    cinfo.enabledExtensionCount = device_extensions<char const*>.size();
    cinfo.ppEnabledExtensionNames = device_extensions<char const*>.data();
    cinfo.enabledLayerCount = 0;

    if (enableValidationLayers) {
        cinfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
        cinfo.ppEnabledLayerNames = validationLayers.data();
    }

    const auto res { vkCreateDevice(this->physical_device, &cinfo, nullptr, &this->logical_device) };
    Throw_On_VkResult_Error("Could not create logical device", res);

    vkGetDeviceQueue(this->logical_device, indices.graphics_family, 0, &this->graphics_queue);
    vkGetDeviceQueue(this->logical_device, indices.present_family, 0, &this->presentation_queue);

    this->memory_layout.Set_Devices(this->physical_device, this->logical_device);
}

void Vk_State::Setup_Swap_Chain() {
    const auto support_details = Get_Swap_Chain_Support_Details(this->physical_device, this->surface);
    const auto surface_format = Choose_Swap_Surface_Format(support_details.formats);
    const auto present_mode = Choose_Swap_Present_Mode(support_details.present_modes);
    int width {}, height {};
    SDL_GetWindowSize(window, &width, &height);
    const auto extent2D = Choose_Swap_Extent(support_details.capabilities, width, height);

    uint32_t imageCount = support_details.capabilities.minImageCount + 1;
    if (support_details.capabilities.maxImageCount > 0 and imageCount > support_details.capabilities.maxImageCount) {
        imageCount = support_details.capabilities.maxImageCount;
    }

    VkSwapchainCreateInfoKHR cinfo {};
    cinfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    cinfo.surface = this->surface;
    cinfo.minImageCount = imageCount;
    cinfo.imageFormat = surface_format.format;
    cinfo.imageColorSpace = surface_format.colorSpace;
    cinfo.imageExtent = extent2D;
    cinfo.imageArrayLayers = 1;
    cinfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;

    const auto indices = Find_Queue_Families(this->physical_device, this->surface);
    std::array queue_family_indices { static_cast<uint32_t>(indices.graphics_family), static_cast<uint32_t>(indices.present_family) };
    if (indices.graphics_family != indices.present_family) {
        cinfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        cinfo.queueFamilyIndexCount = 2;
        cinfo.pQueueFamilyIndices = queue_family_indices.data();
    } else {
        cinfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        cinfo.queueFamilyIndexCount = 0;
        cinfo.pQueueFamilyIndices = nullptr;
    }
    cinfo.preTransform = support_details.capabilities.currentTransform;
    cinfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    cinfo.presentMode = present_mode;
    cinfo.clipped = true;
    cinfo.oldSwapchain = VK_NULL_HANDLE;

    const auto res { vkCreateSwapchainKHR(this->logical_device, &cinfo, nullptr, &this->swap_chain) };
    Throw_On_VkResult_Error("Could not create swapchain", res);

    vkGetSwapchainImagesKHR(this->logical_device,  this->swap_chain, &imageCount, nullptr);
    this->swap_chain_images.resize(imageCount);
    vkGetSwapchainImagesKHR(this->logical_device,  this->swap_chain, &imageCount, this->swap_chain_images.data());

    this->swap_chain_image_format = surface_format.format;
    this->swap_chain_extent = extent2D;
}

void Vk_State::Setup_Swap_Chain_Image_Views() {
    this->swap_chain_image_views.resize(this->swap_chain_images.size());
    for (size_t i { 0 }; i < this->swap_chain_images.size(); ++i) {
        VkImageViewCreateInfo cinfo {};
        cinfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        cinfo.image = this->swap_chain_images[i];
        cinfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
        cinfo.subresourceRange.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        cinfo.subresourceRange.baseMipLevel = 0;
        cinfo.subresourceRange.levelCount = 1;
        cinfo.subresourceRange.baseArrayLayer = 0;
        cinfo.subresourceRange.layerCount = 1;
        cinfo.format = this->swap_chain_image_format;
        cinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;

        const auto create_result { vkCreateImageView(this->logical_device, &cinfo, nullptr, &(this->swap_chain_image_views[i])) };
        if (create_result != VkResult::VK_SUCCESS) {
            throw Vulkan_API_Call_Error { "vkCreateImageView fail: ", create_result };
        }
    }
}

void Vk_State::Setup_Descriptor_Pools() {
    this->Setup_Static_Patch_Ground_Descriptor_Pool();
    this->Setup_Height_Map_Ground_Descriptor_Pool();
}

namespace {
auto Get_Vk_Pipeline_Depth_State_Create_Info_Struct() {
    VkPipelineDepthStencilStateCreateInfo depth_buffer_cinfo {};
    depth_buffer_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depth_buffer_cinfo.depthTestEnable = true;
    depth_buffer_cinfo.depthWriteEnable = true;
    depth_buffer_cinfo.depthCompareOp = VK_COMPARE_OP_LESS;
    depth_buffer_cinfo.depthBoundsTestEnable = true;
    depth_buffer_cinfo.minDepthBounds = 0.0f;
    depth_buffer_cinfo.maxDepthBounds = 1.0f;
    depth_buffer_cinfo.stencilTestEnable = false;
    return depth_buffer_cinfo;
}
}

void Vk_State::Setup_Static_Patch_Ground_Graphics_Pipeline() {
    auto shader_pipeline_set { Create_Shader_Pipeline_Set(this->logical_device, { Static_Patch_Ground_Vert_Shader, Static_Patch_Ground_Vert_Shader_Size },
                                                          { Static_Patch_Ground_Frag_Shader, Static_Patch_Ground_Frag_Shader_Size }, "Static patch ground") };

    const std::array shader_stages_cinfos { shader_pipeline_set.Get_Array_VkPipelineShaderStageCreateInfo("main") };

    std::array<VkVertexInputBindingDescription, 2> vertex_input_bindings {};
    vertex_input_bindings[0].binding = 0;
    vertex_input_bindings[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    vertex_input_bindings[0].stride = sizeof(decltype(scene::static_patch::ground_vertices)::value_type);
    vertex_input_bindings[1].binding = 1;
    vertex_input_bindings[1].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    vertex_input_bindings[1].stride = sizeof(decltype(scene::static_patch::ground_vertices)::value_type);

    std::array<VkVertexInputAttributeDescription, vertex_input_bindings.size()> vertex_input_descriptions {};
    vertex_input_descriptions[0].location = 0;
    vertex_input_descriptions[0].binding = 0;
    vertex_input_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertex_input_descriptions[0].offset = offsetof(scene::static_patch::Grass_Ground_Vertex, scene::static_patch::Grass_Ground_Vertex::position);
    vertex_input_descriptions[1].location = 1;
    vertex_input_descriptions[1].binding = 1;
    vertex_input_descriptions[1].format = VK_FORMAT_R32G32_SFLOAT;
    vertex_input_descriptions[1].offset = offsetof(scene::static_patch::Grass_Ground_Vertex, scene::static_patch::Grass_Ground_Vertex::texture_coordinate);

    VkPipelineVertexInputStateCreateInfo vinput_cinfo {};
    vinput_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vinput_cinfo.flags = 0;
    vinput_cinfo.vertexBindingDescriptionCount = vertex_input_bindings.size();
    vinput_cinfo.pVertexBindingDescriptions = vertex_input_bindings.begin();
    vinput_cinfo.vertexAttributeDescriptionCount = vertex_input_descriptions.size();
    vinput_cinfo.pVertexAttributeDescriptions = vertex_input_descriptions.begin();

    VkPipelineInputAssemblyStateCreateInfo state_cinfo {};
    state_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    state_cinfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    state_cinfo.primitiveRestartEnable = false;

    VkViewport viewport {};
    viewport.x = viewport.y = 0.0f;
    viewport.width = this->swap_chain_extent.width;
    viewport.height = this->swap_chain_extent.height;
    viewport.minDepth = 0;
    viewport.maxDepth = 1;

    VkRect2D scissor = { {0, 0}, this->swap_chain_extent };

    VkPipelineViewportStateCreateInfo viewport_state {};
    viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state.viewportCount = 1;
    viewport_state.pViewports = &viewport;
    viewport_state.scissorCount = 1;
    viewport_state.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = false;
    rasterizer.rasterizerDiscardEnable = false;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = false;
    rasterizer.depthBiasConstantFactor = rasterizer.depthBiasClamp = rasterizer.depthBiasSlopeFactor = 0;

    VkPipelineMultisampleStateCreateInfo multisampling {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = false;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = false;
    multisampling.alphaToOneEnable = false;

    VkPipelineColorBlendAttachmentState attachment {};
    attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    attachment.blendEnable = false;

    VkPipelineColorBlendStateCreateInfo color_blending {};
    color_blending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blending.logicOpEnable = false;
    color_blending.attachmentCount = 1;
    color_blending.pAttachments = &attachment;

    const std::array dynamic_states { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
    [[maybe_unused]] VkPipelineDynamicStateCreateInfo dynamic_state_cinfo {};
    dynamic_state_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state_cinfo.dynamicStateCount = dynamic_states.size();
    dynamic_state_cinfo.pDynamicStates = dynamic_states.data();

    std::array<VkDescriptorSetLayoutBinding, 2> descriptor_bindings {};
    descriptor_bindings[0].binding = 0;
    descriptor_bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    descriptor_bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_bindings[0].descriptorCount = 1;
    descriptor_bindings[0].pImmutableSamplers = nullptr;
    descriptor_bindings[1].binding = 1;
    descriptor_bindings[1].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    descriptor_bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptor_bindings[1].descriptorCount = 1;  // Only one texture so far.
    descriptor_bindings[1].pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo descriptor_layout_cinfo {};
    descriptor_layout_cinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptor_layout_cinfo.flags = 0;
    descriptor_layout_cinfo.bindingCount = descriptor_bindings.size();
    descriptor_layout_cinfo.pBindings = descriptor_bindings.data();

    const VkResult descriptor_layout_res { vkCreateDescriptorSetLayout(this->logical_device, &descriptor_layout_cinfo, nullptr, &this->static_patch.descriptor_layout_ground) };
    Throw_On_VkResult_Error("Could not create a descriptor set layout for vertex shader", descriptor_layout_res);

    VkDescriptorSetAllocateInfo descriptor_allocate_info {};
    descriptor_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_allocate_info.descriptorPool = this->static_patch.descriptor_pool_ground;
    descriptor_allocate_info.descriptorSetCount = 1;
    descriptor_allocate_info.pSetLayouts = &this->static_patch.descriptor_layout_ground;
 
    const VkResult res_descr_set { vkAllocateDescriptorSets(this->logical_device, &descriptor_allocate_info, &this->static_patch.descriptor_set_ground) };
    Throw_On_VkResult_Error("Could not allocate descriptor set", res_descr_set);

    VkDescriptorImageInfo descriptor_image_info {};
    descriptor_image_info.sampler = this->sampler_textures;
    descriptor_image_info.imageView = this->static_patch.ground_grass_texture.image_view;
    descriptor_image_info.imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    VkDescriptorBufferInfo matrix_buffer_info {};
    matrix_buffer_info.buffer = this->matrix_buffer.buffer;
    matrix_buffer_info.offset = 0;
    matrix_buffer_info.range = sizeof(glm::mat4);

    VkWriteDescriptorSet write_descriptor_set_texture {};
    write_descriptor_set_texture.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptor_set_texture.dstSet = this->static_patch.descriptor_set_ground;
    write_descriptor_set_texture.dstBinding = 1;
    write_descriptor_set_texture.dstArrayElement = 0;
    write_descriptor_set_texture.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write_descriptor_set_texture.descriptorCount = 1;
    write_descriptor_set_texture.pImageInfo = &descriptor_image_info;
    write_descriptor_set_texture.pBufferInfo = nullptr;
    write_descriptor_set_texture.pTexelBufferView = nullptr;

    VkWriteDescriptorSet write_descriptor_set_matrix {};
    write_descriptor_set_matrix.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptor_set_matrix.dstSet = this->static_patch.descriptor_set_ground;
    write_descriptor_set_matrix.dstBinding = 0;
    write_descriptor_set_matrix.dstArrayElement = 0;
    write_descriptor_set_matrix.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_descriptor_set_matrix.descriptorCount = 1;
    write_descriptor_set_matrix.pImageInfo = nullptr;
    write_descriptor_set_matrix.pBufferInfo = &matrix_buffer_info;
    write_descriptor_set_matrix.pTexelBufferView = nullptr;

    const std::array write_descriptor_sets { write_descriptor_set_matrix, write_descriptor_set_texture };
    vkUpdateDescriptorSets(this->logical_device, write_descriptor_sets.size(), write_descriptor_sets.data(), 0, nullptr);

    VkPipelineLayoutCreateInfo pipeline_layout_info {};
    pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.setLayoutCount = 1;
    pipeline_layout_info.pSetLayouts = &this->static_patch.descriptor_layout_ground;
    pipeline_layout_info.pushConstantRangeCount = 0;
    pipeline_layout_info.pPushConstantRanges = nullptr;

    const VkResult pipeline_layout_res { vkCreatePipelineLayout(this->logical_device, &pipeline_layout_info, nullptr, &this->static_patch.pipeline_layout_ground) };
    Throw_On_VkResult_Error("Pipeline layout creation failed", pipeline_layout_res);

    const VkPipelineDepthStencilStateCreateInfo depth_buffer_cinfo { Get_Vk_Pipeline_Depth_State_Create_Info_Struct() };

    VkGraphicsPipelineCreateInfo pipeline_cinfo {};
    pipeline_cinfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_cinfo.stageCount = shader_stages_cinfos.size();
    pipeline_cinfo.pStages = shader_stages_cinfos.data();
    pipeline_cinfo.pVertexInputState = &vinput_cinfo;
    pipeline_cinfo.pInputAssemblyState = &state_cinfo;
    pipeline_cinfo.pRasterizationState = &rasterizer;
    pipeline_cinfo.pViewportState = &viewport_state;
    pipeline_cinfo.pMultisampleState = &multisampling;
    pipeline_cinfo.pDepthStencilState = nullptr;
    pipeline_cinfo.pColorBlendState = &color_blending;
    pipeline_cinfo.pDynamicState = nullptr;
    pipeline_cinfo.layout = this->static_patch.pipeline_layout_ground;
    pipeline_cinfo.renderPass = this->render_pass;
    pipeline_cinfo.subpass = 0;
    pipeline_cinfo.basePipelineHandle = VK_NULL_HANDLE;
    pipeline_cinfo.basePipelineIndex = -1;
    pipeline_cinfo.pDepthStencilState = &depth_buffer_cinfo;
    const VkResult pipeline_res { vkCreateGraphicsPipelines(this->logical_device, VK_NULL_HANDLE, 1, &pipeline_cinfo, nullptr, &this->static_patch.graphics_pipeline_ground) };
    Throw_On_VkResult_Error("Failed to create ground graphics pipeline", pipeline_res);
}

void Vk_State::Setup_Height_Map_Graphics_Pipeline() {
    auto shader_pipeline_set {
        Create_Shader_Pipeline_Set(this->logical_device, { Height_Map_Vertex_Shader, Height_Map_Vertex_Shader_Size },
                                                         { Height_Map_TessellationControl_Shader, Height_Map_TessellationControl_Shader_Size },
                                                         { Height_Map_TessellationEvaluation_Shader, Height_Map_TessellationEvaluation_Shader_Size },
                                                         { Height_Map_Fragment_Shader, Height_Map_Fragment_Shader_Size }, "Height map" ) };

    const std::array shader_stages_cinfos { shader_pipeline_set.Get_Array_VkPipelineShaderStageCreateInfo("main") };

    std::array<VkVertexInputBindingDescription, 1> vertex_input_bindings {};
    vertex_input_bindings[0].binding = 0;
    vertex_input_bindings[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    vertex_input_bindings[0].stride = sizeof(scene::Get_Height_Map_Tessellation_Patches().front().front());

    std::array<VkVertexInputAttributeDescription, 1> vertex_input_descriptions {};
    vertex_input_descriptions[0].location = 0;
    vertex_input_descriptions[0].binding = 0;
    vertex_input_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertex_input_descriptions[0].offset = 0;

    VkPipelineVertexInputStateCreateInfo vinput_cinfo {};
    vinput_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vinput_cinfo.flags = 0;
    vinput_cinfo.vertexBindingDescriptionCount = vertex_input_bindings.size();
    vinput_cinfo.pVertexBindingDescriptions = vertex_input_bindings.begin();
    vinput_cinfo.vertexAttributeDescriptionCount = vertex_input_descriptions.size();
    vinput_cinfo.pVertexAttributeDescriptions = vertex_input_descriptions.begin();

    VkPipelineInputAssemblyStateCreateInfo state_cinfo {};
    state_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    state_cinfo.topology = VK_PRIMITIVE_TOPOLOGY_PATCH_LIST;
    state_cinfo.primitiveRestartEnable = false;

    VkViewport viewport {};
    viewport.x = viewport.y = 0.0f;
    viewport.width = this->swap_chain_extent.width;
    viewport.height = this->swap_chain_extent.height;
    viewport.minDepth = 0;
    viewport.maxDepth = 1;

    VkRect2D scissor = { {0, 0}, this->swap_chain_extent };

    VkPipelineViewportStateCreateInfo viewport_state {};
    viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state.viewportCount = 1;
    viewport_state.pViewports = &viewport;
    viewport_state.scissorCount = 1;
    viewport_state.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = false;
    rasterizer.rasterizerDiscardEnable = false;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1;
    rasterizer.cullMode = VK_CULL_MODE_NONE;     // Be able to watch the height maps both from above and below, because why not.
    rasterizer.depthBiasEnable = false;
    rasterizer.depthBiasConstantFactor = 0;
    rasterizer.depthBiasClamp = 0;
    rasterizer.depthBiasSlopeFactor = 0;

    VkPipelineMultisampleStateCreateInfo multisampling {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = false;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = false;
    multisampling.alphaToOneEnable = false;

    VkPipelineColorBlendAttachmentState attachment {};
    attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    attachment.blendEnable = false;

    VkPipelineColorBlendStateCreateInfo color_blending {};
    color_blending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blending.logicOpEnable = false;
    color_blending.attachmentCount = 1;
    color_blending.pAttachments = &attachment;

    const std::array dynamic_states { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
    [[maybe_unused]] VkPipelineDynamicStateCreateInfo dynamic_state_cinfo {};
    dynamic_state_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state_cinfo.dynamicStateCount = dynamic_states.size();
    dynamic_state_cinfo.pDynamicStates = dynamic_states.data();

    std::array<VkDescriptorSetLayoutBinding, 3> descriptor_bindings {};
    descriptor_bindings[0].binding = 0;    // The transformation matrix.
    descriptor_bindings[0].stageFlags = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;  // Only the tessellation evaluation shader will need to create device coordinates.
    descriptor_bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_bindings[0].descriptorCount = 1;
    descriptor_bindings[0].pImmutableSamplers = nullptr;
    descriptor_bindings[1].binding = 1;    // An array of height map heights textures (one 8-bit component per texel)
    descriptor_bindings[1].stageFlags = VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT;
    descriptor_bindings[1].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptor_bindings[1].descriptorCount = scene::Get_Height_Maps().size();
    descriptor_bindings[1].pImmutableSamplers = nullptr;
    descriptor_bindings[2].binding = 2;   // An array of RGB height map textures.
    descriptor_bindings[2].stageFlags = VK_SHADER_STAGE_FRAGMENT_BIT;
    descriptor_bindings[2].descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    descriptor_bindings[2].descriptorCount = scene::Get_Height_Maps().size();
    descriptor_bindings[2].pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo descriptor_layout_cinfo {};
    descriptor_layout_cinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptor_layout_cinfo.flags = 0;
    descriptor_layout_cinfo.bindingCount = descriptor_bindings.size();
    descriptor_layout_cinfo.pBindings = descriptor_bindings.data();

    const VkResult descriptor_layout_res { vkCreateDescriptorSetLayout(this->logical_device, &descriptor_layout_cinfo, nullptr, &this->descriptor_layout_height_map_ground) };
    Throw_On_VkResult_Error("Could not create a descriptor set layout for the height maps pipeline", descriptor_layout_res);

    VkDescriptorSetAllocateInfo descriptor_allocate_info {};
    descriptor_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_allocate_info.descriptorPool = this->descriptor_pool_height_map_ground;
    descriptor_allocate_info.descriptorSetCount = 1;
    descriptor_allocate_info.pSetLayouts = &this->descriptor_layout_height_map_ground;

    const VkResult res_descr_set { vkAllocateDescriptorSets(this->logical_device, &descriptor_allocate_info, &this->descriptor_set_height_map_ground) };
    Throw_On_VkResult_Error("Could not allocate descriptor set", res_descr_set);

    VkDescriptorBufferInfo matrix_buffer_info {};
    matrix_buffer_info.buffer = this->matrix_buffer.buffer;
    matrix_buffer_info.offset = 0;
    matrix_buffer_info.range = sizeof(glm::mat4);

    VkWriteDescriptorSet write_descriptor_set_matrix {};
    write_descriptor_set_matrix.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptor_set_matrix.dstSet = this->descriptor_set_height_map_ground;
    write_descriptor_set_matrix.dstBinding = 0;
    write_descriptor_set_matrix.dstArrayElement = 0;
    write_descriptor_set_matrix.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_descriptor_set_matrix.descriptorCount = 1;
    write_descriptor_set_matrix.pImageInfo = nullptr;
    write_descriptor_set_matrix.pBufferInfo = &matrix_buffer_info;
    write_descriptor_set_matrix.pTexelBufferView = nullptr;

    const auto Images_Info_Vector_To_Descriptor_Vector = [&, this](auto const& vec, VkSampler sampler) {
        const auto Get_Descriptor = [=](Vk_Image_Info image_info) {
            return Get_VkDescriptorImageInfo_For_Shader(image_info.image_view, sampler);
        };
        auto descriptors_view { vec | std::views::transform(Get_Descriptor) };
        return std::vector<VkDescriptorImageInfo> { std::begin(descriptors_view), std::end(descriptors_view) };    // Still no std::ranges::to in GCC 13.
    };
    std::vector<VkDescriptorImageInfo> height_maps_textures_image_info_descriptors { Images_Info_Vector_To_Descriptor_Vector(this->height_maps_textures_images_info, this->sampler_textures) },
      height_maps_image_info_descriptors { Images_Info_Vector_To_Descriptor_Vector(this->height_maps_images_info, this->sampler_height_maps) };

    VkWriteDescriptorSet write_descriptor_set_height_map_textures {};
    write_descriptor_set_height_map_textures.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptor_set_height_map_textures.dstSet = this->descriptor_set_height_map_ground;
    write_descriptor_set_height_map_textures.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write_descriptor_set_height_map_textures.descriptorCount = height_maps_textures_image_info_descriptors.size();
    write_descriptor_set_height_map_textures.dstBinding = 2;
    write_descriptor_set_height_map_textures.pImageInfo = height_maps_textures_image_info_descriptors.data();
    write_descriptor_set_height_map_textures.dstArrayElement = 0;

    VkWriteDescriptorSet write_descriptor_set_height_map_heights {};
    write_descriptor_set_height_map_heights.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptor_set_height_map_heights.dstSet = this->descriptor_set_height_map_ground;
    write_descriptor_set_height_map_heights.descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write_descriptor_set_height_map_heights.descriptorCount = height_maps_image_info_descriptors.size();
    write_descriptor_set_height_map_heights.dstBinding = 1;
    write_descriptor_set_height_map_heights.pImageInfo = height_maps_image_info_descriptors.data();
    write_descriptor_set_height_map_heights.dstArrayElement = 0;

    //const std::array write_descriptor_sets { write_descriptor_set_matrix, write_descriptor_set_height_map_heights, write_descriptor_set_height_map_textures };
    const std::array write_descriptor_sets { write_descriptor_set_matrix, write_descriptor_set_height_map_heights };

    vkUpdateDescriptorSets(this->logical_device, write_descriptor_sets.size(), write_descriptor_sets.begin(), 0, nullptr);
    vkUpdateDescriptorSets(this->logical_device, 1, &write_descriptor_set_height_map_textures, 0, nullptr);

    VkPipelineLayoutCreateInfo pipeline_layout_info {};
    pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.setLayoutCount = 1;
    pipeline_layout_info.pSetLayouts = &this->descriptor_layout_height_map_ground;
    pipeline_layout_info.pushConstantRangeCount = 0;
    pipeline_layout_info.pPushConstantRanges = nullptr;

    const VkResult pipeline_layout_res { vkCreatePipelineLayout(this->logical_device, &pipeline_layout_info, nullptr, &this->pipeline_layout_height_maps) };
    Throw_On_VkResult_Error("Pipeline layout creation failed", pipeline_layout_res);

    const VkPipelineDepthStencilStateCreateInfo depth_buffer_cinfo { Get_Vk_Pipeline_Depth_State_Create_Info_Struct() };

    VkPipelineTessellationStateCreateInfo tessellation_state {};
    tessellation_state.sType = VK_STRUCTURE_TYPE_PIPELINE_TESSELLATION_STATE_CREATE_INFO;
    tessellation_state.patchControlPoints = 4;    // We tessellate rectangular patches.

    VkGraphicsPipelineCreateInfo pipeline_cinfo {};
    pipeline_cinfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_cinfo.stageCount = shader_stages_cinfos.size();
    pipeline_cinfo.pStages = shader_stages_cinfos.data();
    pipeline_cinfo.pVertexInputState = &vinput_cinfo;
    pipeline_cinfo.pInputAssemblyState = &state_cinfo;
    pipeline_cinfo.pRasterizationState = &rasterizer;
    pipeline_cinfo.pViewportState = &viewport_state;
    pipeline_cinfo.pMultisampleState = &multisampling;
    pipeline_cinfo.pDepthStencilState = nullptr;
    pipeline_cinfo.pColorBlendState = &color_blending;
    pipeline_cinfo.pDynamicState = nullptr;
    pipeline_cinfo.layout = this->pipeline_layout_height_maps;
    pipeline_cinfo.renderPass = this->render_pass;
    pipeline_cinfo.subpass = 0;
    pipeline_cinfo.basePipelineHandle = VK_NULL_HANDLE;
    pipeline_cinfo.basePipelineIndex = -1;
    pipeline_cinfo.pDepthStencilState = &depth_buffer_cinfo;
    pipeline_cinfo.pTessellationState = &tessellation_state;
    const VkResult pipeline_res { vkCreateGraphicsPipelines(this->logical_device, VK_NULL_HANDLE, 1, &pipeline_cinfo, nullptr, &this->graphics_pipeline_height_map_ground) };
    Throw_On_VkResult_Error("Failed to create height map graphics pipeline", pipeline_res);
}

void Vk_State::Setup_Graphics_Pipelines() {
    this->Setup_Static_Patch_Blades_Graphics_Pipeline();
    this->Setup_Static_Patch_Ground_Graphics_Pipeline();
    this->Setup_Height_Map_Graphics_Pipeline();
}

void Vk_State::Setup_Static_Patch_Blades_Graphics_Pipeline() {
    auto shader_pipeline_set { Create_Shader_Pipeline_Set(this->logical_device, { Static_Patch_Blade_Vert_Shader, Static_Patch_Blade_Vert_Shader_Size },
                                                          { Static_Patch_Blade_Frag_Shader, Static_Patch_Blade_Frag_Shader_Size }, "Static patch blades ") };
    const std::array shader_stages_cinfos { shader_pipeline_set.Get_Array_VkPipelineShaderStageCreateInfo("main") };

    std::array<VkVertexInputBindingDescription, 2> vertex_input_bindings {};
    vertex_input_bindings[0].binding = 0;
    vertex_input_bindings[0].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    vertex_input_bindings[0].stride = sizeof(decltype(scene::static_patch::grass_blade_vertices)::value_type);
    vertex_input_bindings[1].binding = 1;
    vertex_input_bindings[1].inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    vertex_input_bindings[1].stride = sizeof(decltype(scene::static_patch::grass_blade_vertices)::value_type);

    std::array<VkVertexInputAttributeDescription, vertex_input_bindings.size()> vertex_input_descriptions {};
    vertex_input_descriptions[0].location = 0;
    vertex_input_descriptions[0].binding = 0;
    vertex_input_descriptions[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertex_input_descriptions[0].offset = offsetof(scene::static_patch::Grass_Blade_Vertex, scene::static_patch::Grass_Blade_Vertex::position);
    vertex_input_descriptions[1].location = 1;
    vertex_input_descriptions[1].binding = 1;
    vertex_input_descriptions[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    vertex_input_descriptions[1].offset = offsetof(scene::static_patch::Grass_Blade_Vertex, scene::static_patch::Grass_Blade_Vertex::color);

    VkPipelineVertexInputStateCreateInfo vinput_cinfo {};
    vinput_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vinput_cinfo.flags = 0;
    vinput_cinfo.vertexBindingDescriptionCount = vertex_input_bindings.size();
    vinput_cinfo.pVertexBindingDescriptions = vertex_input_bindings.begin();
    vinput_cinfo.vertexAttributeDescriptionCount = vertex_input_descriptions.size();
    vinput_cinfo.pVertexAttributeDescriptions = vertex_input_descriptions.begin();

    VkPipelineInputAssemblyStateCreateInfo state_cinfo {};
    state_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    state_cinfo.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    state_cinfo.primitiveRestartEnable = false;

    VkViewport viewport {};
    viewport.x = viewport.y = 0.0f;
    viewport.width = this->swap_chain_extent.width;
    viewport.height = this->swap_chain_extent.height;
    viewport.minDepth = 0;
    viewport.maxDepth = 1;

    VkRect2D scissor = { {0, 0}, this->swap_chain_extent };

    VkPipelineViewportStateCreateInfo viewport_state {};
    viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state.viewportCount = 1;
    viewport_state.pViewports = &viewport;
    viewport_state.scissorCount = 1;
    viewport_state.pScissors = &scissor;

    VkPipelineRasterizationStateCreateInfo rasterizer {};
    rasterizer.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizer.depthClampEnable = false;
    rasterizer.rasterizerDiscardEnable = false;
    rasterizer.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizer.lineWidth = 1;
    rasterizer.cullMode = VK_CULL_MODE_BACK_BIT;    // This was a mistake, there should have been no culling, instead of duplicating vertex data for both sides of a blade.
    rasterizer.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizer.depthBiasEnable = false;
    rasterizer.depthBiasConstantFactor = rasterizer.depthBiasClamp = rasterizer.depthBiasSlopeFactor = 0;

    VkPipelineMultisampleStateCreateInfo multisampling {};
    multisampling.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampling.sampleShadingEnable = false;
    multisampling.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    multisampling.minSampleShading = 1.0f;
    multisampling.pSampleMask = nullptr;
    multisampling.alphaToCoverageEnable = false;
    multisampling.alphaToOneEnable = false;

    VkPipelineColorBlendAttachmentState attachment {};
    attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    attachment.blendEnable = false;

    VkPipelineColorBlendStateCreateInfo color_blending {};
    color_blending.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    color_blending.logicOpEnable = false;
    color_blending.attachmentCount = 1;
    color_blending.pAttachments = &attachment;

    const std::array dynamic_states { VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_LINE_WIDTH };
    [[maybe_unused]] VkPipelineDynamicStateCreateInfo dynamic_state_cinfo {};
    dynamic_state_cinfo.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state_cinfo.dynamicStateCount = dynamic_states.size();
    dynamic_state_cinfo.pDynamicStates = dynamic_states.data();

    std::array<VkDescriptorSetLayoutBinding, 1> descriptor_bindings;
    descriptor_bindings[0].binding = 0;
    descriptor_bindings[0].stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    descriptor_bindings[0].descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    descriptor_bindings[0].descriptorCount = 1;
    descriptor_bindings[0].pImmutableSamplers = nullptr;

    VkDescriptorSetLayoutCreateInfo descriptor_layout_cinfo {};
    descriptor_layout_cinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptor_layout_cinfo.flags = 0;
    descriptor_layout_cinfo.bindingCount = descriptor_bindings.size();
    descriptor_layout_cinfo.pBindings = descriptor_bindings.data();

    const VkResult descriptor_layout_res { vkCreateDescriptorSetLayout(this->logical_device, &descriptor_layout_cinfo, nullptr, &this->static_patch.descriptor_layout_blades) };
    Throw_On_VkResult_Error("Could not create a descriptor set layout for vertex shader", descriptor_layout_res);

    VkDescriptorSetAllocateInfo descriptor_allocate_info {};
    descriptor_allocate_info.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    descriptor_allocate_info.descriptorPool = this->static_patch.descriptor_pool_blades;
    descriptor_allocate_info.descriptorSetCount = 1;
    descriptor_allocate_info.pSetLayouts = &this->static_patch.descriptor_layout_blades;

    const VkResult res_descr_set { vkAllocateDescriptorSets(this->logical_device, &descriptor_allocate_info, &this->static_patch.descriptor_set_blades) };
    Throw_On_VkResult_Error("Could not allocate descriptor set", res_descr_set);

    VkDescriptorBufferInfo matrix_buffer_info {};
    matrix_buffer_info.buffer = this->matrix_buffer.buffer;
    matrix_buffer_info.offset = 0;
    matrix_buffer_info.range = sizeof(glm::mat4);

    VkWriteDescriptorSet write_descriptor_set {};
    write_descriptor_set.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write_descriptor_set.dstSet = this->static_patch.descriptor_set_blades;
    write_descriptor_set.dstBinding = 0;
    write_descriptor_set.dstArrayElement = 0;
    write_descriptor_set.descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write_descriptor_set.descriptorCount = 1;
    write_descriptor_set.pImageInfo = nullptr;
    write_descriptor_set.pBufferInfo = &matrix_buffer_info;
    write_descriptor_set.pTexelBufferView = nullptr;

    vkUpdateDescriptorSets(this->logical_device, 1, &write_descriptor_set, 0, nullptr);

    VkPipelineLayoutCreateInfo pipeline_layout_info {};
    pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.setLayoutCount = 1;
    pipeline_layout_info.pSetLayouts = &this->static_patch.descriptor_layout_blades;
    pipeline_layout_info.pushConstantRangeCount = 0;
    pipeline_layout_info.pPushConstantRanges = nullptr;

    const VkResult pipeline_layout_res { vkCreatePipelineLayout(this->logical_device, &pipeline_layout_info, nullptr, &this->static_patch.pipeline_layout_blades) };
    Throw_On_VkResult_Error("Pipeline layout creation failed", pipeline_layout_res);

    const VkPipelineDepthStencilStateCreateInfo depth_buffer_cinfo { Get_Vk_Pipeline_Depth_State_Create_Info_Struct() };

    VkGraphicsPipelineCreateInfo pipeline_cinfo {};
    pipeline_cinfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_cinfo.stageCount = shader_stages_cinfos.size();
    pipeline_cinfo.pStages = shader_stages_cinfos.data();
    pipeline_cinfo.pVertexInputState = &vinput_cinfo;
    pipeline_cinfo.pInputAssemblyState = &state_cinfo;
    pipeline_cinfo.pRasterizationState = &rasterizer;
    pipeline_cinfo.pViewportState = &viewport_state;
    pipeline_cinfo.pMultisampleState = &multisampling;
    pipeline_cinfo.pDepthStencilState = nullptr;
    pipeline_cinfo.pColorBlendState = &color_blending;
    pipeline_cinfo.pDynamicState = nullptr;
    pipeline_cinfo.layout = this->static_patch.pipeline_layout_blades;
    pipeline_cinfo.renderPass = this->render_pass;
    pipeline_cinfo.subpass = 0;
    pipeline_cinfo.basePipelineHandle = VK_NULL_HANDLE;
    pipeline_cinfo.basePipelineIndex = -1;
    pipeline_cinfo.pDepthStencilState = &depth_buffer_cinfo;
    const VkResult pipeline_res { vkCreateGraphicsPipelines(this->logical_device, VK_NULL_HANDLE, 1, &pipeline_cinfo, nullptr, &this->static_patch.graphics_pipeline_blades) };
    Throw_On_VkResult_Error("Failed to create blades graphics pipeline", pipeline_res);
}

void Vk_State::Setup_Render_Pass() {
    VkAttachmentDescription color_attachment {};
    color_attachment.format = this->swap_chain_image_format;
    color_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    color_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    color_attachment.storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    color_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    color_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    color_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    color_attachment.finalLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

    VkAttachmentDescription depth_buffer_attachment {};
    depth_buffer_attachment.format = VK_FORMAT_D32_SFLOAT;
    depth_buffer_attachment.samples = VK_SAMPLE_COUNT_1_BIT;
    depth_buffer_attachment.loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    depth_buffer_attachment.storeOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth_buffer_attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    depth_buffer_attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    depth_buffer_attachment.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    depth_buffer_attachment.finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;

    VkAttachmentReference color_attachment_ref {};
    color_attachment_ref.attachment = 0;
    color_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    VkAttachmentReference depth_buffer_attachment_ref {};
    depth_buffer_attachment_ref.attachment = 1;
    depth_buffer_attachment_ref.layout = VK_IMAGE_LAYOUT_DEPTH_ATTACHMENT_OPTIMAL;

    std::array attachments { color_attachment, depth_buffer_attachment };

    VkSubpassDescription subpass_descr {};
    subpass_descr.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass_descr.colorAttachmentCount = 1;
    subpass_descr.pColorAttachments = &color_attachment_ref;
    subpass_descr.pDepthStencilAttachment = &depth_buffer_attachment_ref;

    VkSubpassDependency dependancy {};
    dependancy.srcSubpass = VK_SUBPASS_EXTERNAL;
    dependancy.dstSubpass = 0;
    dependancy.srcStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    dependancy.dstStageMask = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT | VK_PIPELINE_STAGE_EARLY_FRAGMENT_TESTS_BIT;
    dependancy.srcAccessMask = 0;
    dependancy.dstAccessMask = VK_ACCESS_COLOR_ATTACHMENT_READ_BIT | VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT | VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT;

    std::array subpass_dependencies { dependancy };

    VkRenderPassCreateInfo renderpass_cinfo {};
    renderpass_cinfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    renderpass_cinfo.attachmentCount = attachments.size();
    renderpass_cinfo.pAttachments = attachments.data();
    renderpass_cinfo.subpassCount = 1;
    renderpass_cinfo.pSubpasses = &subpass_descr;
    renderpass_cinfo.dependencyCount = subpass_dependencies.size();
    renderpass_cinfo.pDependencies = subpass_dependencies.data();

    const auto render_pass_creation_res { vkCreateRenderPass(this->logical_device, &renderpass_cinfo, nullptr, &this->render_pass) };
    Throw_On_VkResult_Error("Render pass creation failed", render_pass_creation_res);
}

void Vk_State::Setup_Framebuffers() {
    this->swap_chain_framebuffers.resize(this->swap_chain_image_views.size());

    for (size_t i { 0 }; i < this->swap_chain_image_views.size(); ++i) {
        std::array attachments { this->swap_chain_image_views[i], this->depth_buffer.Get_Image_View() };

        VkFramebufferCreateInfo frame_cinfo {};
        frame_cinfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        frame_cinfo.renderPass = this->render_pass;
        frame_cinfo.attachmentCount = attachments.size();
        frame_cinfo.pAttachments = attachments.data();
        frame_cinfo.width = this->swap_chain_extent.width;
        frame_cinfo.height = this->swap_chain_extent.height;
        frame_cinfo.layers = 1;

        const auto frame_res { vkCreateFramebuffer(this->logical_device, &frame_cinfo, nullptr, &this->swap_chain_framebuffers[i]) };
        Throw_On_VkResult_Error("Framebuffer creation failure", frame_res);
    }
}

void Vk_State::Setup_Graphics_Command_Pool() {
    const auto indices { Find_Queue_Families(this->physical_device, this->surface) };
    VkCommandPoolCreateInfo pool_cinfo {};
    pool_cinfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    pool_cinfo.queueFamilyIndex = indices.graphics_family;
    pool_cinfo.flags = 0;

    const auto pool_res { vkCreateCommandPool(this->logical_device, &pool_cinfo, nullptr, &this->graphics_command_pool) };
    Throw_On_VkResult_Error("Command pool creation failed", pool_res);
}

void Vk_State::Setup_Command_Buffers() {
    this->command_buffers_render_pass_swap_chain.resize(this->swap_chain_framebuffers.size());

    VkCommandBufferAllocateInfo alloc_info {};
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.commandPool = this->graphics_command_pool;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = static_cast<uint32_t>(this->command_buffers_render_pass_swap_chain.size());
    const VkResult alloc_res { vkAllocateCommandBuffers(this->logical_device, &alloc_info, this->command_buffers_render_pass_swap_chain.data()) };
    Throw_On_VkResult_Error("Command buffer creation failure", alloc_res);

    for (size_t i { 0 }; i < this->command_buffers_render_pass_swap_chain.size(); ++i) {
        VkCommandBufferBeginInfo begin_info {};
        begin_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
        begin_info.flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT;
        begin_info.pInheritanceInfo = nullptr;

        const VkResult begin_res { vkBeginCommandBuffer(this->command_buffers_render_pass_swap_chain[i], &begin_info) };
        Throw_On_VkResult_Error("Could not begin render pass command buffer", begin_res);

        const VkClearColorValue clear_color { 0.0f, 0.0f, 0.0f, 1.0f };
        const VkClearDepthStencilValue depth_clear_value { .depth=1.0, .stencil=0 };
        const std::array<VkClearValue, 2> clear_values { VkClearValue { .color=clear_color }, VkClearValue { .depthStencil=depth_clear_value } };
        VkRenderPassBeginInfo render_info {};
        render_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
        render_info.renderPass = this->render_pass;
        render_info.framebuffer = this->swap_chain_framebuffers[i];
        render_info.renderArea.offset = {0, 0};
        render_info.renderArea.extent = this->swap_chain_extent;
        render_info.clearValueCount = clear_values.size();
        render_info.pClearValues = clear_values.data();
        vkCmdBeginRenderPass(this->command_buffers_render_pass_swap_chain[i], &render_info, VK_SUBPASS_CONTENTS_INLINE);
        this->Add_Draw_Commands_For_Static_Patch(i);
        this->Add_Draw_Commands_For_Height_Maps_Rendering(i);
        vkCmdEndRenderPass(this->command_buffers_render_pass_swap_chain[i]);

        const VkResult end_res { vkEndCommandBuffer(this->command_buffers_render_pass_swap_chain[i]) };
        Throw_On_VkResult_Error("Failed to end command buffer", end_res);
    }
}

void Vk_State::Add_Draw_Commands_For_Static_Patch(size_t i) {
    // Draw ground
    if (true)
    {
        const std::array buffers_to_bind { this->static_patch.ground_vertex_buffer.buffer, this->static_patch.ground_vertex_buffer.buffer };
        const std::array<VkDeviceSize, buffers_to_bind.size()> offsets_buffers { 0, 0 };
        vkCmdBindPipeline(this->command_buffers_render_pass_swap_chain[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->static_patch.graphics_pipeline_ground);
        vkCmdBindDescriptorSets(this->command_buffers_render_pass_swap_chain[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->static_patch.pipeline_layout_ground,
                                0, 1, &this->static_patch.descriptor_set_ground, 0, nullptr);
        vkCmdBindVertexBuffers(this->command_buffers_render_pass_swap_chain[i], 0, buffers_to_bind.size(), buffers_to_bind.begin(), offsets_buffers.begin());
        vkCmdDraw(this->command_buffers_render_pass_swap_chain[i], scene::static_patch::ground_vertices.size(), 1, 0, 0);
    }
    else std::cerr << "NOT drawing ground!\n";
    // Draw grass blades
    if (true)
    {
        const std::array buffers_to_bind { this->static_patch.blades_vertex_buffer.buffer, this->static_patch.blades_vertex_buffer.buffer };
        const std::array<VkDeviceSize, buffers_to_bind.size()> offsets_buffers { 0, 0 };
        vkCmdBindPipeline(this->command_buffers_render_pass_swap_chain[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->static_patch.graphics_pipeline_blades);
        vkCmdBindDescriptorSets(this->command_buffers_render_pass_swap_chain[i], VK_PIPELINE_BIND_POINT_GRAPHICS, this->static_patch.pipeline_layout_blades,
                                0, 1, &this->static_patch.descriptor_set_blades, 0, nullptr);
        vkCmdBindVertexBuffers(this->command_buffers_render_pass_swap_chain[i], 0, buffers_to_bind.size(), buffers_to_bind.begin(), offsets_buffers.begin());
        vkCmdDraw(this->command_buffers_render_pass_swap_chain[i], scene::static_patch::grass_blade_vertices.size(), 1, 0, 0);
    }
    else std::cerr << "NOT drawing blades!\n";
}

void Vk_State::Add_Draw_Commands_For_Height_Maps_Rendering(size_t command_buffer_index) {
    VkCommandBuffer command_buffer { this->command_buffers_render_pass_swap_chain[command_buffer_index] };
    const std::array buffers_to_bind { this->height_maps_patches_vertex_buffer.buffer };
    const std::array<VkDeviceSize, buffers_to_bind.size()> offsets { 0 };
    vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this->graphics_pipeline_height_map_ground);
    vkCmdBindDescriptorSets(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, this->pipeline_layout_height_maps, 0, 1, &this->descriptor_set_height_map_ground, 0, nullptr);
    vkCmdBindVertexBuffers(command_buffer, 0, buffers_to_bind.size(), buffers_to_bind.begin(), offsets.begin());
    vkCmdDraw(command_buffer, Get_Size_Of_Range_Of_Ranges(scene::Get_Height_Map_Tessellation_Patches()), 1, 0, 0);
}

void Vk_State::Setup_Semaphores() {
    VkSemaphoreCreateInfo semaphore_cinfo {};
    semaphore_cinfo.sType =  VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    const auto first_res { vkCreateSemaphore(this->logical_device, &semaphore_cinfo, nullptr, &this->semaphore_render_finished) };
    const auto second_res { vkCreateSemaphore(this->logical_device, &semaphore_cinfo, nullptr, &this->semaphore_image_available) };
    Throw_On_VkResult_Error("Semaphore creation failure", first_res);
    Throw_On_VkResult_Error("Semaphore creation failure", second_res);
}

void Vk_State::Notify_Window_Resized() {
    this->depth_buffer.Recreate_Buffer();
    this->Recreate_Swap_Chain();
}

void Vk_State::Draw_Frame() {
    if (enableValidationLayers) {
        vkQueueWaitIdle(this->presentation_queue);
    }
    this->Copy_Uniform_Matrix_To_Buffer();

    uint32_t image_index {};
    const auto result = vkAcquireNextImageKHR(this->logical_device, this->swap_chain, std::numeric_limits<uint64_t>::max(), this->semaphore_image_available, VK_NULL_HANDLE, &image_index);
    if (result == VK_ERROR_OUT_OF_DATE_KHR) {
        std::cout << "Swap chain outdated, recreating it" << std::endl;
        this->Recreate_Swap_Chain();
        return;
    }
    else if (result != VK_SUCCESS and result != VK_SUBOPTIMAL_KHR) {
        throw Vulkan_API_Call_Error { "Could not acquire next image from swap chain: ", result };
    }

    VkSubmitInfo submit_info {};
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;

    const std::array wait_semaphores { this->semaphore_image_available };
    std::array<VkPipelineStageFlags, 1> wait_stages { VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT };
    submit_info.waitSemaphoreCount = wait_semaphores.size();
    submit_info.pWaitSemaphores = wait_semaphores.data();
    submit_info.pWaitDstStageMask = wait_stages.data();
    submit_info.commandBufferCount = wait_stages.size();
    submit_info.pCommandBuffers = &this->command_buffers_render_pass_swap_chain[image_index];
    std::array signal_semaphores { this->semaphore_render_finished };
    submit_info.signalSemaphoreCount = signal_semaphores.size();
    submit_info.pSignalSemaphores = signal_semaphores.data();

    const auto submit_res { vkQueueSubmit(graphics_queue, 1, &submit_info, VK_NULL_HANDLE) };
    Throw_On_VkResult_Error("Failed to submit command buffers to the graphics queue", submit_res);

    VkPresentInfoKHR present_info {};
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.waitSemaphoreCount = 1;
    present_info.pWaitSemaphores = signal_semaphores.data();

    const std::array swapchains { this->swap_chain };
    const std::array indices { image_index };
    present_info.swapchainCount = swapchains.size();
    present_info.pSwapchains = swapchains.data();
    present_info.pImageIndices = indices.data();
    present_info.pResults = nullptr;

    vkQueuePresentKHR(this->presentation_queue, &present_info);
}

void Vk_State::Wait_End_Asynchronous_Operations() {
    vkDeviceWaitIdle(this->logical_device);
}

void Vk_State::Recreate_Swap_Chain() {
    this->Wait_End_Asynchronous_Operations();
    this->Clean_Up_Swap_Chain();
    this->Setup_Swap_Chain();
    this->Setup_Swap_Chain_Image_Views();
    this->Setup_Render_Pass();
    this->Setup_Descriptor_Pools();
    this->Setup_Graphics_Pipelines();
    this->Setup_Framebuffers();
    this->Setup_Command_Buffers();
}

void Vk_State::Clean_Up_Swap_Chain() {
    vkFreeDescriptorSets(this->logical_device, this->static_patch.descriptor_pool_ground, 1, &this->static_patch.descriptor_set_ground);
    vkFreeDescriptorSets(this->logical_device, this->static_patch.descriptor_pool_blades, 1, &this->static_patch.descriptor_set_blades);
    vkFreeDescriptorSets(this->logical_device, this->descriptor_pool_height_map_ground, 1, &this->descriptor_set_height_map_ground);

    vkDestroyDescriptorPool(this->logical_device, this->static_patch.descriptor_pool_ground, nullptr);
    vkDestroyDescriptorPool(this->logical_device, this->static_patch.descriptor_pool_blades, nullptr);
    vkDestroyDescriptorPool(this->logical_device, this->descriptor_pool_height_map_ground, nullptr);

    vkDestroyDescriptorSetLayout(this->logical_device, this->static_patch.descriptor_layout_ground, nullptr);
    vkDestroyDescriptorSetLayout(this->logical_device, this->static_patch.descriptor_layout_blades, nullptr);
    vkDestroyDescriptorSetLayout(this->logical_device, this->descriptor_layout_height_map_ground, nullptr);

    for (const auto framebuffer: this->swap_chain_framebuffers) {
        vkDestroyFramebuffer(this->logical_device, framebuffer, nullptr);
    }
    vkFreeCommandBuffers(this->logical_device, this->graphics_command_pool, static_cast<uint32_t>(this->command_buffers_render_pass_swap_chain.size()), this->command_buffers_render_pass_swap_chain.data());

    vkDestroyPipeline(this->logical_device, this->static_patch.graphics_pipeline_blades, nullptr);
    vkDestroyPipeline(this->logical_device, this->static_patch.graphics_pipeline_ground, nullptr);
    vkDestroyPipeline(this->logical_device, this->graphics_pipeline_height_map_ground, nullptr);

    vkDestroyPipelineLayout(this->logical_device, this->static_patch.pipeline_layout_blades, nullptr);
    vkDestroyPipelineLayout(this->logical_device, this->static_patch.pipeline_layout_ground, nullptr);
    vkDestroyPipelineLayout(this->logical_device, this->pipeline_layout_height_maps, nullptr);

    vkDestroyRenderPass(this->logical_device, this->render_pass, nullptr);
    for (auto const& iv: this->swap_chain_image_views) {
        vkDestroyImageView(this->logical_device, iv, nullptr);
    }
    vkDestroySwapchainKHR(this->logical_device, this->swap_chain, nullptr);
}

std::optional<uint32_t> Get_Host_Visible_Device_Memory_Type(VkPhysicalDevice const& physical_device) {
    VkPhysicalDeviceMemoryProperties2 mem_props2 {};
    mem_props2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
    vkGetPhysicalDeviceMemoryProperties2(physical_device, &mem_props2);
    for (unsigned int i { 0 }; i < mem_props2.memoryProperties.memoryTypeCount; i++) {
        const auto type = mem_props2.memoryProperties.memoryTypes[i];
        if (type.propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT and type.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT and
            type.propertyFlags & VK_MEMORY_PROPERTY_HOST_COHERENT_BIT) {
            return i;
        }
    }
    return std::nullopt;
}

void Vk_State::Allocate_Device_Memory() {
    this->memory_layout.Allocate_Memory();

    this->static_patch.blades_vertex_buffer.Bind_Buffer_Memory(this->logical_device);
    this->static_patch.ground_vertex_buffer.Bind_Buffer_Memory(this->logical_device);
    this->height_maps_patches_vertex_buffer.Bind_Buffer_Memory(this->logical_device);
    this->matrix_buffer.Bind_Buffer_Memory(this->logical_device);

    Copy_Array_To_Mapped_Pointer(scene::static_patch::grass_blade_vertices, this->static_patch.blades_vertex_buffer.memory_slice->Get_Host_Mapped_Memory_Pointer());
    Copy_Array_To_Mapped_Pointer(scene::static_patch::ground_vertices, this->static_patch.ground_vertex_buffer.memory_slice->Get_Host_Mapped_Memory_Pointer());

    // Upload the height maps's data (patches vertices) to GPU memory

    auto height_maps_patches { scene::Get_Height_Map_Tessellation_Patches() };
    char* height_maps_patches_host_buffer_pointer { this->height_maps_patches_vertex_buffer.memory_slice->Get_Host_Mapped_Memory_Pointer() };
    for (auto height_map_patch: height_maps_patches) {
        Copy_Array_To_Mapped_Pointer(height_map_patch, height_maps_patches_host_buffer_pointer);
        height_maps_patches_host_buffer_pointer += Span_Byte_Size(height_map_patch);
    }

    this->static_patch.ground_grass_texture.Bind_Image_Memory(this->logical_device);

    std::vector height_maps { scene::Get_Height_Maps_Textures_Sizes() };
    for (size_t i { 0 }; i < height_maps.size(); i++) {
        this->height_maps_textures_images_info[i].Bind_Image_Memory(this->logical_device);
        this->height_maps_images_info[i].Bind_Image_Memory(this->logical_device);
    }
}

Vk_Buffer_Info Vk_State::Create_Buffer(unsigned int byte_size, VkBufferUsageFlagBits usage, bool exclusive) {
    Vk_Buffer_Info buffer_info {};

    VkBufferCreateInfo vertex_buffer_cinfo {};
    vertex_buffer_cinfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    vertex_buffer_cinfo.size = byte_size;
    vertex_buffer_cinfo.usage = usage;
    if (exclusive) {
        vertex_buffer_cinfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    }
    else {
        vertex_buffer_cinfo.sharingMode = VK_SHARING_MODE_CONCURRENT;
    }
    const auto vertex_res_create { vkCreateBuffer(this->logical_device, &vertex_buffer_cinfo, nullptr, &buffer_info.buffer) };
    if (vertex_res_create != VK_SUCCESS) {
        throw Vulkan_API_Call_Error { "Could not create vertex array buffer" };
    }
    return buffer_info;
}

void Vk_State::Setup_Vertex_Buffers() {
    this->static_patch.ground_vertex_buffer = this->Create_Buffer(Vector_Byte_Size(scene::static_patch::ground_vertices), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, true);
    this->static_patch.blades_vertex_buffer = this->Create_Buffer(Vector_Byte_Size(scene::static_patch::grass_blade_vertices), VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, true);

    const size_t height_maps_total_size { Get_Byte_Size_Of_Range_Of_Ranges(scene::Get_Height_Map_Tessellation_Patches()) };
    this->height_maps_patches_vertex_buffer = this->Create_Buffer(height_maps_total_size, VK_BUFFER_USAGE_VERTEX_BUFFER_BIT, true);

    this->static_patch.ground_vertex_buffer.memory_slice = this->memory_layout.Reserve_Vertex_Array(this->static_patch.ground_vertex_buffer.buffer);
    this->static_patch.blades_vertex_buffer.memory_slice = this->memory_layout.Reserve_Vertex_Array(this->static_patch.blades_vertex_buffer.buffer);
    this->height_maps_patches_vertex_buffer.memory_slice = this->memory_layout.Reserve_Vertex_Array(this->height_maps_patches_vertex_buffer.buffer);
}

void Vk_State::Setup_Samplers() {
    VkSamplerCreateInfo sampler_cinfo {};
    sampler_cinfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    sampler_cinfo.flags = 0;
    sampler_cinfo.magFilter = VK_FILTER_LINEAR;
    sampler_cinfo.minFilter = VK_FILTER_LINEAR;
    sampler_cinfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    sampler_cinfo.addressModeU = sampler_cinfo.addressModeV = sampler_cinfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    sampler_cinfo.mipLodBias = 0;
    sampler_cinfo.anisotropyEnable = false;
    sampler_cinfo.compareEnable = false;
    sampler_cinfo.minLod = 0;
    sampler_cinfo.maxLod = VK_LOD_CLAMP_NONE;
    sampler_cinfo.borderColor = VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK;
    sampler_cinfo.unnormalizedCoordinates = false;

    VkResult res_create_sampler { vkCreateSampler(this->logical_device, &sampler_cinfo, nullptr, &this->sampler_textures) };
    Throw_On_VkResult_Error("Could not create texture sampler", res_create_sampler);

    sampler_cinfo.magFilter = VK_FILTER_NEAREST;
    sampler_cinfo.minFilter = VK_FILTER_NEAREST;
    sampler_cinfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_NEAREST;

    res_create_sampler = vkCreateSampler(this->logical_device, &sampler_cinfo, nullptr, &this->sampler_height_maps);
    Throw_On_VkResult_Error("Could not create texture sampler for height maps", res_create_sampler);
}

void Vk_State::Setup_Texture_Images() {
    const auto [width, height] = scene::Get_Texture_Size();
    this->static_patch.ground_grass_texture.image = Create_Simple_2D_VkImage(this->logical_device, width, height, this->format_rgba_textures);
    this->static_patch.ground_grass_texture.memory_slice = this->memory_layout.Reserve_Texture(this->static_patch.ground_grass_texture.image);

    std::vector height_maps_textures { scene::Get_Height_Maps_Textures_Sizes() };
    this->height_maps_textures_images_info.clear();
    this->height_maps_textures_images_info.resize(height_maps_textures.size());
    for (size_t i { 0 }; i < height_maps_textures.size(); i++) {
        const auto [width, height] = height_maps_textures[i];
        this->height_maps_textures_images_info[i].image = Create_Simple_2D_VkImage(this->logical_device, width, height, this->format_rgba_textures);
        this->height_maps_textures_images_info[i].memory_slice = this->memory_layout.Reserve_Texture(this->height_maps_textures_images_info[i].image);
    }

    std::vector height_maps { scene::Get_Height_Maps() };
    this->height_maps_images_info.resize(height_maps.size());
    for (size_t i { 0 }; i < height_maps_textures.size(); i++) {
        const VkFormat format { VK_FORMAT_R8_UINT };    // Height map only have heights that can range from 0 to 255 at most.
        const auto [width, height] = height_maps_textures[i];
        this->height_maps_images_info[i].image = Create_Simple_2D_VkImage(this->logical_device, width, height, format);
        this->height_maps_images_info[i].memory_slice = this->memory_layout.Reserve_Texture(this->height_maps_images_info[i].image);
    }
}

void Vk_State::Setup_Texture_Image_Views() {
    this->static_patch.ground_grass_texture.image_view = Create_Simple_2D_VkImageView(this->logical_device, this->static_patch.ground_grass_texture.image, this->format_rgba_textures);

    // Setup the RGB textures and heights textures for the height maps.
    std::vector height_maps { scene::Get_Height_Maps_Textures_Sizes() };
    for (size_t i { 0 }; i < height_maps.size(); i++) {
        this->height_maps_textures_images_info[i].image_view = Create_Simple_2D_VkImageView(this->logical_device, this->height_maps_textures_images_info[i].image, this->format_rgba_textures);
        this->height_maps_images_info[i].image_view = Create_Simple_2D_VkImageView(this->logical_device, this->height_maps_images_info[i].image, VK_FORMAT_R8_UINT);
    }
}

std::optional<uint32_t> Get_Host_Memory_Type(VkPhysicalDevice physical_device) {
    VkPhysicalDeviceMemoryProperties2 mem_props2 {};
    mem_props2.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MEMORY_PROPERTIES_2;
    vkGetPhysicalDeviceMemoryProperties2(physical_device, &mem_props2);
    for (unsigned int i { 0 }; i < mem_props2.memoryProperties.memoryTypeCount; i++) {
        const auto type = mem_props2.memoryProperties.memoryTypes[i];
        if (type.propertyFlags & VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) {
            return i;
        }
    }
    return std::nullopt;
}

void Vk_State::Copy_Textures_To_Images() {
    Vulkan_Texture_Copy_Job_Scheduler texture_copy_scheduler {};
    texture_copy_scheduler.Set_Device(this->physical_device, this->logical_device, this->surface);

    const auto [width, height] = scene::Get_Texture_Size();
    texture_copy_scheduler.Add_Image_To_Transfer(scene::Get_Ground_Texture_Data(), this->static_patch.ground_grass_texture.image, width, height);

    std::vector height_maps_sizes { scene::Get_Height_Maps_Textures_Sizes() };
    std::vector height_maps_textures_data { scene::Get_Height_Maps_Texture_Pixels() };
    std::vector height_maps_heights_data { scene::Get_Height_Maps() };
    for (size_t i { 0 }; i < height_maps_sizes.size(); i++) {
        const auto [width, height] = height_maps_sizes[i];
        texture_copy_scheduler.Add_Image_To_Transfer(height_maps_textures_data[i], this->height_maps_textures_images_info[i].image, width, height);
        texture_copy_scheduler.Add_Image_To_Transfer(std::span<const char>{reinterpret_cast<char const*>(height_maps_heights_data[i].data()), height_maps_heights_data[i].size()},
                                                     this->height_maps_images_info[i].image, width, height);
    }

    texture_copy_scheduler.Start_Copying_Images();
    texture_copy_scheduler.Wait_For_Copying_To_End();
    texture_copy_scheduler.Destroy();
}

void Vk_State::Setup_Static_Patch_Ground_Descriptor_Pool() {
    std::array<VkDescriptorPoolSize, 2> pool_sizes_ground {};
    pool_sizes_ground[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    pool_sizes_ground[0].descriptorCount = 1;
    pool_sizes_ground[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    pool_sizes_ground[1].descriptorCount = 1;   // Only one texture so far.

    VkDescriptorPoolCreateInfo descriptor_pool_cinfo {};
    descriptor_pool_cinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_cinfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptor_pool_cinfo.maxSets = 1;
    descriptor_pool_cinfo.poolSizeCount = pool_sizes_ground.size();
    descriptor_pool_cinfo.pPoolSizes = pool_sizes_ground.data();

    const VkResult res_descr_pool { vkCreateDescriptorPool(this->logical_device, &descriptor_pool_cinfo, nullptr, &this->static_patch.descriptor_pool_ground) };
    Throw_On_VkResult_Error("Could not create descriptor pool for grass ground", res_descr_pool);

    std::array<VkDescriptorPoolSize, 1> pool_sizes_blades {};
    pool_sizes_blades[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    pool_sizes_blades[0].descriptorCount = 1;

    VkDescriptorPoolCreateInfo descriptor_pool_cinfo_blades {};
    descriptor_pool_cinfo_blades.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_cinfo_blades.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptor_pool_cinfo_blades.maxSets = 1;
    descriptor_pool_cinfo_blades.poolSizeCount = pool_sizes_blades.size();
    descriptor_pool_cinfo_blades.pPoolSizes = pool_sizes_blades.data();

    const VkResult res_descr_pool_blades { vkCreateDescriptorPool(this->logical_device, &descriptor_pool_cinfo_blades, nullptr, &this->static_patch.descriptor_pool_blades) };
    Throw_On_VkResult_Error("Could not create descriptor pool for grass blades", res_descr_pool_blades);
}

void Vk_State::Setup_Height_Map_Ground_Descriptor_Pool() {
    std::array<VkDescriptorPoolSize, 3> pool_sizes_ground {};
    pool_sizes_ground[0].type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    pool_sizes_ground[0].descriptorCount = 1;
    pool_sizes_ground[1].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    pool_sizes_ground[1].descriptorCount = scene::Get_Height_Maps().size(); // The heights texture.
    pool_sizes_ground[2].type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    pool_sizes_ground[2].descriptorCount = scene::Get_Height_Maps().size(); // The height map's texture.

    VkDescriptorPoolCreateInfo descriptor_pool_cinfo {};
    descriptor_pool_cinfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptor_pool_cinfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptor_pool_cinfo.maxSets = 1;
    descriptor_pool_cinfo.poolSizeCount = pool_sizes_ground.size();
    descriptor_pool_cinfo.pPoolSizes = pool_sizes_ground.data();

    const VkResult res_descr_pool { vkCreateDescriptorPool(this->logical_device, &descriptor_pool_cinfo, nullptr, &this->descriptor_pool_height_map_ground) };
    Throw_On_VkResult_Error("Could not create descriptor pool for height map ground", res_descr_pool);
}

void Vk_State::Setup_Depth_Buffer() {
    this->depth_buffer = Vk_Depth_Buffer { this->physical_device, this->logical_device, this->window };
}

void Vk_State::Setup_Matrix_Buffer() {
    this->matrix_buffer = this->Create_Buffer(sizeof(glm::mat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, true);
    this->matrix_buffer.memory_slice = this->memory_layout.Reserve_Vertex_Array(this->matrix_buffer.buffer);
}

void Vk_State::Copy_Uniform_Matrix_To_Buffer() {
    char* const pointer_matrix { reinterpret_cast<char*>(&scene::projection_matrix) };
    std::copy(pointer_matrix, pointer_matrix + sizeof(scene::projection_matrix),
          this->matrix_buffer.memory_slice->Get_Host_Mapped_Memory_Pointer());
}
