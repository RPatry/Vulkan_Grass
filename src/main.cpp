#include <iostream>
#include <memory>
#include <vector>
#include <string>
#include <algorithm>
#include <iomanip>
#include <filesystem>
#include <sstream>
#include <optional>

#include "SDL2/SDL.h"
#include "SDL2/SDL_vulkan.h"
#include "vulkan/vulkan.h"

#include "vk.hpp"
#include "vk_validation.hpp"
#include "vk_extensions.hpp"
#include "utils.hpp"
#include "scene.hpp"

namespace {
struct SDL_Window_Deleter {
    void operator()(SDL_Window* window) {
        SDL_DestroyWindow(window);
    }
};
using SDL_Window_Pointer = std::unique_ptr<SDL_Window, SDL_Window_Deleter>;

SDL_Window_Pointer Create_Window(int width, int height) {
    SDL_Window* window { SDL_CreateWindow(
        "Vulkan Grass", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,
        width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_VULKAN | SDL_WINDOW_RESIZABLE) };
    return SDL_Window_Pointer { window, SDL_Window_Deleter {} };
}

bool Is_Key_Pressed(SDL_Scancode code) {
    return static_cast<bool>(SDL_GetKeyboardState(nullptr)[code]);
}

extern "C" {
    extern const char Grass_Ground_Texture [];
    extern const unsigned int Grass_Ground_Texture_Size;
}

void Set_Window_Icon(SDL_Window* window) {
    auto grass { Read_PNG_File_From_Memory(std::span<const char> { Grass_Ground_Texture, Grass_Ground_Texture_Size }) };
    auto const icon_surface {SDL_CreateRGBSurfaceFrom(
        grass.pixels.data(),
        static_cast<int>(grass.width), static_cast<int>(grass.height),
        32, 4 * grass.width, 0xFF, 0xFF00, 0xFF0000, 0xFF000000) };
    SDL_SetWindowIcon(window, icon_surface);

    SDL_FreeSurface(icon_surface);

}

void Show_Error_Dialog(std::string const& error_message) {
    SDL_MessageBoxButtonData button_data {};
    button_data.flags = SDL_MESSAGEBOX_BUTTON_RETURNKEY_DEFAULT;
    button_data.buttonid = 0;
    button_data.text = "OK";

    SDL_MessageBoxData data {};
    data.flags = SDL_MESSAGEBOX_ERROR;
    data.title = "Error!";
    data.message = error_message.c_str();
    data.numbuttons = 1;
    data.buttons = &button_data;
    data.colorScheme = nullptr;
    if (SDL_ShowMessageBox(&data, nullptr) != 0) {
        // The stderr is a failback if the message box could not be shown.
        std::cerr << error_message << std::endl;
    }
}

void Print_Keyboard_Controls() {
    std::cout << "Keyboard controls:" << std::endl <<
                 "- Up/Down/Left/Right: move forwards/backwards/left/right" << std::endl <<
                 "- Ctrl+Left/Right/Up/Down: rotate camera" << std::endl <<
                 "- R: reset camera" << std::endl;
}
}

int main([[maybe_unused]] int argv, [[maybe_unused]] char** argc) {
    SDL_version version {};
    SDL_GetVersion(&version);
    std::cout << "Starting program (running SDL version " << (int)version.major << '.' << (int)version.minor << '.' << (int)version.patch << ')' << std::endl;
    scene::Load_Textures();
    scene::static_patch::Create_Static_Patch();
    scene::Create_Patches();

    const auto init_code { SDL_Init(SDL_INIT_VIDEO) };
    if (init_code != 0) {
        Show_Error_Dialog(std::string { "Could not initialize SDL: " } + SDL_GetError());
        return 1;
    }

    int width { 750 }, height { 750 };
    auto window = Create_Window(width, height);
    if (window.get() == nullptr) {
        Show_Error_Dialog(std::string { "Could not create SDL window: " } + SDL_GetError());
        return 1;
    }
    scene::Set_Window_Size(width, height);

    Set_Window_Icon(window.get());

    SDL_ShowWindow(window.get());

    std::optional<Vk_State> vk_state {};
    try {
        vk_state.emplace(window.get());
    }
    catch (Vulkan_API_Call_Error e) {
        std::ostringstream error_message_stream;
        error_message_stream << "Vulkan_API_Call_Error caught: " << e.error_message;
        Show_Error_Dialog(error_message_stream.str());
        return 1;
    }

    Print_Keyboard_Controls();

    SDL_Event event {};
    while (true) {
        SDL_PollEvent(&event);
        if ((event.type == SDL_QUIT) or
            (event.type == SDL_KEYDOWN and Is_Key_Pressed(SDL_SCANCODE_ESCAPE))) {
            break;
        }
        else if ((event.type == SDL_WINDOWEVENT) and (event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED)) {
            vk_state->Wait_End_Asynchronous_Operations();
            SDL_GetWindowSize(window.get(), &width, &height);
            scene::Set_Window_Size(width, height);
            vk_state->Notify_Window_Resized();
        }
        if (Is_Key_Pressed(SDL_SCANCODE_UP)) {
            if (Is_Key_Pressed(SDL_SCANCODE_LCTRL) or Is_Key_Pressed(SDL_SCANCODE_RCTRL)) {
                scene::Move_Camera_Up();
            }
            else {
                scene::Advance_Camera();
            }
        }
        else if (Is_Key_Pressed(SDL_SCANCODE_DOWN)) {
            if (Is_Key_Pressed(SDL_SCANCODE_LCTRL) or Is_Key_Pressed(SDL_SCANCODE_RCTRL)) {
                scene::Move_Camera_Down();
            }
            else {
                scene::Move_Camera_Backwards();
            }
        }
        else if (Is_Key_Pressed(SDL_SCANCODE_LEFT)) {
            if (Is_Key_Pressed(SDL_SCANCODE_LCTRL) or Is_Key_Pressed(SDL_SCANCODE_RCTRL)) {
                scene::Move_Camera_Left();
            }
            else {
                scene::Advance_Camera_Left();
            }
        }
        else if (Is_Key_Pressed(SDL_SCANCODE_RIGHT)) {
            if (Is_Key_Pressed(SDL_SCANCODE_LCTRL) or Is_Key_Pressed(SDL_SCANCODE_RCTRL)) {
                scene::Move_Camera_Right();
            }
            else {
                scene::Advance_Camera_Right();
            }
        }
        else if (Is_Key_Pressed(SDL_SCANCODE_R)) {
            scene::Reset_Camera();
        }
        vk_state->Draw_Frame();
        scene::Update_Time_Spent();
    }
    vk_state->Wait_End_Asynchronous_Operations();

    SDL_Quit();

    return 0;
}
