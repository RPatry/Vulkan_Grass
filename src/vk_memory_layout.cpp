#include "vk_memory_layout.hpp"
#include <iostream>
#include "vk_exceptions.hpp"
#include "math.hpp"

namespace {
int Get_Memory_Type_Index(VkPhysicalDevice physical_device, int property) {
    VkPhysicalDeviceMemoryProperties memory_properties {};
    vkGetPhysicalDeviceMemoryProperties(physical_device, &memory_properties);
    for (uint32_t i { 0 }; i < memory_properties.memoryTypeCount; i++) {
        if (memory_properties.memoryTypes[i].propertyFlags & property) {
            return i;
        }
    }
    return -1;
}
}

template <bool Host_Mappable>
Vk_Memory_Slice<Host_Mappable>::Vk_Memory_Slice(unsigned int offset, unsigned int size, Memory_Allocation_Info& memory) :
    offset { offset }, size { size }, memory { &memory }
{}

template <bool Host_Mappable>
char* Vk_Memory_Slice<Host_Mappable>::Get_Host_Mapped_Memory_Pointer() requires (Host_Mappable) {
    if (this->memory->host_mapped_pointer == nullptr) {
        void* host_mapped_pointer {};
        const auto map_result { vkMapMemory(this->memory->parent_memory_layout->logical_device, this->memory->memory, 0, VK_WHOLE_SIZE, 0, &host_mapped_pointer) };
        Throw_On_VkResult_Error("Could not map memory to host", map_result);
        this->memory->host_mapped_pointer = reinterpret_cast<char*>(host_mapped_pointer);
    }
    return this->memory->host_mapped_pointer + this->offset;
}

template <bool Host_Mappable>
std::span<char> Vk_Memory_Slice<Host_Mappable>::Get_Host_Mapped_Memory_Span() requires (Host_Mappable) {
    return std::span { this->Get_Host_Mapped_Memory_Pointer(), this->size };
}

template
char* Vk_Memory_Slice<true>::Get_Host_Mapped_Memory_Pointer();
template
std::span<char> Vk_Memory_Slice<true>::Get_Host_Mapped_Memory_Span();

void Vk_Memory_Layout::Set_Devices(VkPhysicalDevice physical_device, VkDevice logical_device) {
    this->physical_device = physical_device;
    this->logical_device = logical_device;
}

/*
Vk_Memory_Layout::Vk_Memory_Layout(Vk_Memory_Layout&& layout)
{
    this->Deallocate();
    this->physical_device = layout.physical_device;
    this->logical_device = layout.logical_device;
    this->vertex_array_memory = layout.vertex_array_memory;
}
*/

void Vk_Memory_Layout::Deallocate() {
    // vkFreeMemory accepts null handles for the VkDeviceMemory
    vkFreeMemory(this->logical_device, this->vertex_array_memory.memory, nullptr);
    vkFreeMemory(this->logical_device, this->texture_memory.memory, nullptr);
    this->vertex_array_memory.memory = VK_NULL_HANDLE;
    this->texture_memory.memory = VK_NULL_HANDLE;
}

[[nodiscard]]
Vk_Buffer_Memory_Slice Vk_Memory_Layout::Reserve_Vertex_Array(VkBuffer buffer) {
    if (this->memory_allocated) {
        throw Vk_Memory_Already_Allocated {};
    }
    VkMemoryRequirements requirements {};
    vkGetBufferMemoryRequirements(this->logical_device, buffer, &requirements);
    const unsigned int offset_new_buffer { math::Get_Next_Aligned_Offset(this->vertex_array_memory.offset_next_object, requirements.alignment) };
    this->vertex_array_memory.offset_next_object = offset_new_buffer + requirements.size;
    return Vk_Buffer_Memory_Slice { offset_new_buffer, static_cast<unsigned int>(requirements.size), this->vertex_array_memory };
}

[[nodiscard]]
Vk_Image_Memory_Slice Vk_Memory_Layout::Reserve_Texture(VkImage image) {
    if (this->memory_allocated) {
        throw Vk_Memory_Already_Allocated {};
    }
    VkMemoryRequirements requirements {};
    vkGetImageMemoryRequirements(this->logical_device, image, &requirements);
    const unsigned int offset_new_buffer { math::Get_Next_Aligned_Offset(this->texture_memory.offset_next_object, requirements.alignment) };
    this->texture_memory.offset_next_object = offset_new_buffer + requirements.size;
    return Vk_Image_Memory_Slice { offset_new_buffer, static_cast<unsigned int>(requirements.size), this->texture_memory };
}

void Vk_Memory_Layout::Allocate_Memory_For_Type(Memory_Allocation_Info& memory_type_object, int memory_type_index) {
    if (memory_type_object.offset_next_object == 0) {
        return;
    }
    VkMemoryAllocateInfo allocate_info {};
    allocate_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocate_info.allocationSize = memory_type_object.offset_next_object;
    allocate_info.memoryTypeIndex = memory_type_index;
    const auto res_allocate_memory { vkAllocateMemory(this->logical_device, &allocate_info, nullptr, &memory_type_object.memory) };
    Throw_On_VkResult_Error("Could not allocate memory", res_allocate_memory);
}

void Vk_Memory_Layout::Allocate_Memory() {
    if (this->memory_allocated) {
        throw Vk_Memory_Already_Allocated {};
    }

    const auto vertex_memory_type_index { Get_Memory_Type_Index(this->physical_device, VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT) };
    const auto texture_memory_type_index { Get_Memory_Type_Index(this->physical_device, VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) };
    this->Allocate_Memory_For_Type(this->vertex_array_memory, vertex_memory_type_index);
    this->Allocate_Memory_For_Type(this->texture_memory, texture_memory_type_index);

    this->memory_allocated = true;
}
