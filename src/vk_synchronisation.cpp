#include "vk_synchronisation.hpp"
#include "vk_exceptions.hpp"

VkFence Create_New_VkFence(VkDevice device, bool create_signalled) {
    VkFence new_fence {};

    VkFenceCreateInfo fence_cinfo {};
    fence_cinfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    if (create_signalled) {
        fence_cinfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;
    }
    fence_cinfo.pNext = nullptr;

    const VkResult res_create_fence { vkCreateFence(device, &fence_cinfo, nullptr, &new_fence) };
    Throw_On_VkResult_Error("Could not create VkFence", res_create_fence);

    return new_fence;
}