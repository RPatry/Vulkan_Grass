 # Store all textures directly inside the executable

        .section .rodata
	.global Grass_Ground_Texture
	.align 8
Grass_Ground_Texture:
	.incbin "textures/grass.png"
Grass_Ground_Texture_End:	

	.global Grass_Ground_Texture_Size
	.align 8
Grass_Ground_Texture_Size:
	.int Grass_Ground_Texture_End - Grass_Ground_Texture

    .global Height_Map_Test
	.align 8
Height_Map_Test:
    .incbin "heightmaps/hm2.png"
Height_Map_Test_End:

    .global Height_Map_Test_Size
	.align 8
Height_Map_Test_Size:
    .int Height_Map_Test_End - Height_Map_Test

    .global Height_Map_2
	.align 8
Height_Map_2:
    .incbin "heightmaps/hm3.png"
Height_Map_2_End:

    .global Height_Map_2_Size
	.align 8
Height_Map_2_Size:
    .int Height_Map_2_End - Height_Map_2
