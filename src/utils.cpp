#include "utils.hpp"
#include <png.h>
#include <cstdio>
#include <memory>
#include <algorithm>
#include <iostream>
#include <fstream>
#include <set>

std::vector<char> Read_Binary_File(std::string_view path_file) {
    std::ifstream the_file { std::string { path_file }, std::ios::ate | std::ios::binary };
    const size_t file_size = the_file.tellg();
    std::vector<char> bytes (file_size);
    the_file.seekg(0);
    the_file.read(bytes.data(), file_size);

    return bytes;
}

struct Png_Ptr_Deleter {
    Png_Ptr_Deleter() = default;
    void operator()(png_structp png_ptr) {
        png_destroy_read_struct(&png_ptr, &info_ptr, nullptr);
    }
    png_infop info_ptr {};
};

namespace {
void Read_PNG_Rows(Image_File_Data& result, auto rows, int color_type) {
    const unsigned int row_size { result.width * 4 };
    result.pixels.resize(row_size * result.height);
    std::ranges::fill(result.pixels, 0xFF);
    if (color_type == PNG_COLOR_TYPE_RGB_ALPHA) {
        for (unsigned int y { 0 }; y < result.height; y++) {
            std::copy(rows[y], rows[y] + row_size, result.pixels.data() + (y * row_size));
        }
    }
    else {
        // Assume RGB channels with 8-bit samples.
        for (unsigned int y { 0 }; y < result.height; y++) {
            for (unsigned int x { 0 }; x < result.width; x++) {
            char unsigned* const source_row_r_sample_pixel { rows[y] + x * 3 };
            char* const target_row_r_sample_pixel { result.pixels.data() + (y * row_size) + (x * 4) };
            std::copy(source_row_r_sample_pixel, source_row_r_sample_pixel + 3,
                  target_row_r_sample_pixel);
            }
        }
    }
}
}

Image_File_Data Read_PNG_File(char const* path_png_file) {
    Image_File_Data result;
    // "rb" mode necessary on Windows per https://stackoverflow.com/questions/16764429/libpng-error-read-error-visual-studio-2010
    // Otherwise, libpng fails with a Read error
    std::unique_ptr<FILE, decltype(&std::fclose)> ptr_file { std::fopen(path_png_file, "rb"), &fclose };
    Png_Ptr_Deleter png_ptr_deleter {};
    std::unique_ptr<png_struct, Png_Ptr_Deleter> png_ptr { png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr), png_ptr_deleter };
    auto* const ptr_info_struct { png_create_info_struct(png_ptr.get()) };
    png_ptr_deleter.info_ptr = ptr_info_struct;
    png_init_io(png_ptr.get(), ptr_file.get());
    png_read_png(png_ptr.get(), ptr_info_struct, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_GRAY_TO_RGB, nullptr);

    int color_type {};
    png_get_IHDR(png_ptr.get(), ptr_info_struct, &result.width, &result.height, nullptr, &color_type, nullptr, nullptr, nullptr);

    const auto rows { png_get_rows(png_ptr.get(), ptr_info_struct) };
    Read_PNG_Rows(result, rows, color_type);

    return result;
}

namespace {
struct Span_Reader_Info {
    std::span<char const>& span_binary_data;
    unsigned int current_position_span { 0 };
};

//! The custom reading handler that allows libpng to read a PNG file's bytes from memory instead of from the disk.
void Read_PNG_Bytes_From_Span(png_structp png_ptr, png_bytep data, png_size_t length) {
    Span_Reader_Info* span_reader_info { static_cast<Span_Reader_Info*>(png_get_io_ptr(png_ptr)) };
    const auto nb_bytes_to_read { std::min(length, span_reader_info->span_binary_data.size() - span_reader_info->current_position_span) }; 
    std::copy(span_reader_info->span_binary_data.begin() + span_reader_info->current_position_span,
          span_reader_info->span_binary_data.begin() + span_reader_info->current_position_span + nb_bytes_to_read,
          data);
    span_reader_info->current_position_span += nb_bytes_to_read;
}
}

Image_File_Data Read_PNG_File_From_Memory(std::span<char const> binary_data) {
    Image_File_Data result;
    Span_Reader_Info span_reader_info { .span_binary_data=binary_data };
    Png_Ptr_Deleter png_ptr_deleter {};
    std::unique_ptr<png_struct, Png_Ptr_Deleter> png_ptr { png_create_read_struct(PNG_LIBPNG_VER_STRING, nullptr, nullptr, nullptr), png_ptr_deleter };
    auto* const ptr_info_struct { png_create_info_struct(png_ptr.get()) };
    png_ptr_deleter.info_ptr = ptr_info_struct;
    png_set_read_fn(png_ptr.get(), &span_reader_info, &Read_PNG_Bytes_From_Span);
    png_read_png(png_ptr.get(), ptr_info_struct, PNG_TRANSFORM_STRIP_16 | PNG_TRANSFORM_GRAY_TO_RGB, nullptr);

    int color_type {};
    png_get_IHDR(png_ptr.get(), ptr_info_struct, &result.width, &result.height, nullptr, &color_type, nullptr, nullptr, nullptr);

    const auto rows { png_get_rows(png_ptr.get(), ptr_info_struct) };
    Read_PNG_Rows(result, rows, color_type);

    return result;
}

Height_Map_Data Read_Height_Map_From_PNG(std::span<char const> binary_data_png_file, bool normalize_heights) {
    Image_File_Data texture_data { Read_PNG_File_From_Memory(binary_data_png_file) };
    Height_Map_Data result { .width = texture_data.width, .height = texture_data.height };
    for (size_t i { 0 }; i < result.width * result.height; i++) {
        const size_t index_color { i * 4 };
        const uint8_t r { static_cast<uint8_t>(texture_data.pixels[index_color]) };
        const uint8_t g { static_cast<uint8_t>(texture_data.pixels[index_color + 1]) };
        const uint8_t b { static_cast<uint8_t>(texture_data.pixels[index_color + 2]) };
        const uint8_t gray { static_cast<uint8_t>(0.2126 * r + 0.7152 * g + 0.0722 * b) };
        result.heights.push_back(gray);
    }
    if (normalize_heights) {
        // Flatten the differences between grayscale values, but keep their relative order, that is:
        // Map all gray values to the interval [0; N-1], where N is the total number of gray values found in the height maps.
        // For the case where every single gray value from 0 to 255 is already found in a heightmap, this obviously leaves the heights vector unchanged.
        std::set<uint8_t> all_heights { result.heights.begin(), result.heights.end() };
        std::vector<uint8_t> all_heights_sorted { all_heights.begin(), all_heights.end() };
        for (uint8_t& gray: result.heights) {
            gray = std::distance(all_heights_sorted.begin(), std::ranges::find(all_heights_sorted, gray));
        }
    }
    return result;
}

uint8_t Height_Map_Data::Get_Max_Height() const {
    return *std::ranges::max_element(this->heights);
}
