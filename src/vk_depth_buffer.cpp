#include "vk_depth_buffer.hpp"
#include "vk_exceptions.hpp"

Vk_Depth_Buffer::Vk_Depth_Buffer(VkPhysicalDevice physical_device, VkDevice logical_device, SDL_Window* window) :
    physical_device { physical_device }, logical_device { logical_device }, window { window }
{
    this->Create_Image();
    this->Allocate_Memory();
    this->Bind_Image_To_Memory();
    this->Create_Image_View();
}

VkImageView Vk_Depth_Buffer::Get_Image_View() const {
    return this->depth_buffer_image_view;
}

void Vk_Depth_Buffer::Recreate_Buffer() {
    // Cleanup the previous resources.
    vkDestroyImageView(this->logical_device, this->depth_buffer_image_view, nullptr);
    vkDestroyImage(this->logical_device, this->depth_buffer_image, nullptr);

    this->Create_Image();
    this->Reallocate_Memory_If_Required();
    this->Bind_Image_To_Memory();
    this->Create_Image_View();
}

void Vk_Depth_Buffer::Create_Image() {
    int new_screen_width, new_screen_height;
    SDL_GetWindowSize(this->window, &new_screen_width, &new_screen_height);

    VkImageCreateInfo image_cinfo {};
    image_cinfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_cinfo.extent = { static_cast<uint32_t>(new_screen_width), static_cast<uint32_t>(new_screen_height), 1 };
    image_cinfo.format = VK_FORMAT_D32_SFLOAT;
    image_cinfo.flags = 0;
    image_cinfo.imageType = VK_IMAGE_TYPE_2D;
    image_cinfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    image_cinfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    image_cinfo.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    image_cinfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    image_cinfo.samples = VK_SAMPLE_COUNT_1_BIT;
    image_cinfo.mipLevels = 1;
    image_cinfo.arrayLayers = 1;

    const auto res { vkCreateImage(this->logical_device, &image_cinfo, nullptr, &this->depth_buffer_image) };
    Throw_On_VkResult_Error("Could not create image for depth buffer", res);
}

void Vk_Depth_Buffer::Reallocate_Memory_If_Required() {
    VkMemoryRequirements requirements {};
    vkGetImageMemoryRequirements(this->logical_device, this->depth_buffer_image, &requirements);
    if (requirements.size > this->allocation_size) {
        vkFreeMemory(this->logical_device, this->depth_buffer_memory, nullptr);
        this->Allocate_Memory();
    }
}

namespace {
int Get_Memory_Type_Index_For_Depth_Buffer(VkPhysicalDevice physical_device, VkMemoryRequirements requirements) {
    VkPhysicalDeviceMemoryProperties memory_properties {};
    vkGetPhysicalDeviceMemoryProperties(physical_device, &memory_properties);
    for (uint32_t i { 0 }; i < memory_properties.memoryTypeCount; i++) {
        if ((memory_properties.memoryTypes[i].propertyFlags & VK_MEMORY_PROPERTY_DEVICE_LOCAL_BIT) and
            (i & requirements.memoryTypeBits)) {
            return i;
        }
    }
    return -1;
}
}

void Vk_Depth_Buffer::Allocate_Memory() {
    VkMemoryRequirements requirements {};
    vkGetImageMemoryRequirements(this->logical_device, this->depth_buffer_image, &requirements);

    VkMemoryAllocateInfo allocation_info {};
    allocation_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    allocation_info.allocationSize = requirements.size;
    allocation_info.memoryTypeIndex = Get_Memory_Type_Index_For_Depth_Buffer(this->physical_device, requirements);

    const auto alloc_res { vkAllocateMemory(this->logical_device, &allocation_info, nullptr, &this->depth_buffer_memory) };
    Throw_On_VkResult_Error("Could not reallocate more memory for the depth buffer", alloc_res);
    this->allocation_size = requirements.size;
}

void Vk_Depth_Buffer::Create_Image_View() {
    VkImageSubresourceRange range {};
    range.baseMipLevel = 0;
    range.baseArrayLayer = 0;
    range.levelCount = 1;
    range.layerCount = 1;
    range.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;

    VkImageViewCreateInfo image_view_cinfo {};
    image_view_cinfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    image_view_cinfo.format = VK_FORMAT_D32_SFLOAT;
    image_view_cinfo.viewType = VK_IMAGE_VIEW_TYPE_2D;
    image_view_cinfo.subresourceRange = range;
    image_view_cinfo.flags = 0;
    image_view_cinfo.components.r =
        image_view_cinfo.components.g =
        image_view_cinfo.components.b =
        image_view_cinfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    image_view_cinfo.image = this->depth_buffer_image;

    const auto res_image_view { vkCreateImageView(this->logical_device, &image_view_cinfo, nullptr, &this->depth_buffer_image_view) };
    Throw_On_VkResult_Error("Could not create image for depth buffer", res_image_view);
}

void Vk_Depth_Buffer::Bind_Image_To_Memory() {
    const auto res_bind_depth_buffer { vkBindImageMemory(this->logical_device, this->depth_buffer_image, this->depth_buffer_memory, 0) };
    Throw_On_VkResult_Error("Could not bind depth buffer image", res_bind_depth_buffer);
}

void Vk_Depth_Buffer::Clean_Resources() {
    vkDestroyImageView(this->logical_device, this->depth_buffer_image_view, nullptr);
    vkDestroyImage(this->logical_device, this->depth_buffer_image, nullptr);
    vkFreeMemory(this->logical_device, this->depth_buffer_memory, nullptr);
}
