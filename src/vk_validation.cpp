#include "vk_validation.hpp"
#include <algorithm>
#include <iterator>
#include <ranges>

bool Check_Layer_Validation_Support() {
    const auto available { Get_Validation_Layers_Available() };

    for (auto layer: validationLayers) {
        bool layer_found { false };
        for (auto const& layer_prop: available) {
            if (layer_prop.name == layer) {
                layer_found = true;
                break;
            }
        }
        if (not layer_found) {
            return false;
	}
    }
    return true;
}

std::ostream& operator<<(std::ostream& os, Validation_Layer const& vl) {
    return os << vl.name << " : " << vl.description;
}

std::vector<Validation_Layer> Get_Validation_Layers_Available() {
    uint32_t layer_count {};
    vkEnumerateInstanceLayerProperties(&layer_count, nullptr);
    std::vector<VkLayerProperties> available(layer_count);
    vkEnumerateInstanceLayerProperties(&layer_count, available.data());

    const auto get_validation_layer = [](auto const& layer) {
	return Validation_Layer { layer.layerName, layer.description };
    };
    auto view_validation_layers = available | std::views::transform(get_validation_layer);
    return std::vector<Validation_Layer> { view_validation_layers.begin(), view_validation_layers.end() };
}

void Test_Show_Validation_Layers() noexcept {
    const auto layers { Get_Validation_Layers_Available() };
    std::cout << "Validation layers:" << std::endl;
    for (auto layer: layers) {
	std::cout << layer << std::endl;
    }
}
