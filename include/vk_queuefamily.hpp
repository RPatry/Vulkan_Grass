#ifndef VULKAN_GRASS_VK_QUEUEFAMILY_HPP
#define VULKAN_GRASS_VK_QUEUEFAMILY_HPP

#include <vulkan/vulkan.h>
#include <vector>
#include <set>

struct Queue_Family_Indices {
    int graphics_family { -1 };
    int present_family { -1 };
    int compute_family { -1 };
    int transfer_family { -1 };
    int transfer_whole_images_family { -1 };

    bool Is_Complete() const {
        return (this->graphics_family >= 0) and (this->present_family >= 0) and
               (this->compute_family >= 0) and (this->transfer_family >= 0) and
               (this->transfer_whole_images_family >= 0);
    }

    std::set<int> Get_All_Unique_Families() const {
        return std::set<int>{ this->graphics_family, this->present_family,
                              this->compute_family, this->transfer_family,
                              this->transfer_whole_images_family };
    }
};

Queue_Family_Indices Find_Queue_Families(VkPhysicalDevice device, VkSurfaceKHR surface);

std::vector<VkQueueFamilyProperties> Find_All_Queue_Families_Properties(VkPhysicalDevice device);

int Find_Queue_Family_For_Transfer(VkPhysicalDevice device);

#endif // VULKAN_GRASS_VK_QUEUEFAMILY_HPP
