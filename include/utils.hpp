#ifndef VULKAN_GRASS_UTILS_HPP
#define VULKAN_GRASS_UTILS_HPP

#include <vector>
#include <string_view>
#include <span>
#include <cstdint>
#include <algorithm>
#include <ranges>

std::vector<char> Read_Binary_File(std::string_view path_file);

struct Image_File_Data {
    /** @brief A row-major matrix storing RGBA data for all pixels, with one byte per channel.
        @details Its size is (width x height x 4). */
    std::vector<char> pixels {};
    unsigned int width { 0 };
    unsigned int height { 0 };
};

Image_File_Data Read_PNG_File(char const* file_path_png_file);

Image_File_Data Read_PNG_File_From_Memory(std::span<char const> binary_data_png_file);

struct Height_Map_Data {
    std::vector<uint8_t> heights {};
    unsigned int width { 0 };
    unsigned int height { 0 };
    uint8_t Get_Max_Height() const;
};

Height_Map_Data Read_Height_Map_From_PNG(std::span<char const> binary_data_png_file, bool normalize_gray);

template <typename T>
auto Vector_Byte_Size(std::vector<T> const& vec) {
    return vec.size() * sizeof(T);
}

template <typename T>
auto Span_Byte_Size(std::span<T> span) {
    return span.size() * sizeof(T);
}

template <typename T1, typename T2>
void Copy_Array_To_Mapped_Pointer(std::span<T1> array, T2* target_pointer, unsigned int byte_offset = 0) {
    char* const target_pointer_bytes { reinterpret_cast<char*>(target_pointer) + byte_offset };
    char const* const source_pointer_bytes { reinterpret_cast<char const*>(array.data()) };
    std::copy(source_pointer_bytes,
	      source_pointer_bytes + Span_Byte_Size(array),
	      target_pointer_bytes);
}

template <typename T1, typename T2>
void Copy_Array_To_Mapped_Pointer(std::vector<T1> const& array, T2* target_pointer, unsigned int byte_offset = 0) {
    Copy_Array_To_Mapped_Pointer(std::span<const T1> { array }, target_pointer, byte_offset);
}

//! Functor to add a size_t with the size of a standard container.
struct Add_Sizes {
template <typename Sized_Range>
size_t operator()(size_t current_size, Sized_Range const& range) {
    return current_size + std::size(range);
}
};

template <std::ranges::input_range Range>
requires (std::ranges::sized_range<std::ranges::range_value_t<Range>>)
size_t Get_Size_Of_Range_Of_Ranges(Range&& range) {
    return std::ranges::fold_left(range, 0, Add_Sizes{});
}

//! Returns the total byte size of all the inner arrays of a span-like of span-likes<T>.
template <std::ranges::input_range Range>
requires (std::ranges::sized_range<std::ranges::range_value_t<Range>>)
size_t Get_Byte_Size_Of_Range_Of_Ranges(Range&& range) {
    using Inner_Array_Type = std::ranges::range_value_t<Range>;
    using Innermost_Element_Type = std::ranges::range_value_t<Inner_Array_Type>;
    return std::ranges::fold_left(range, 0, Add_Sizes{}) * sizeof(Innermost_Element_Type);
}

#endif // VULKAN_GRASS_UTILS_HPP
