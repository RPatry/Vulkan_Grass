#ifndef VULKAN_GRASS_VK_HPP
#define VULKAN_GRASS_VK_HPP

#include "SDL2/SDL.h"
#include "SDL2/SDL_vulkan.h"
#include <vulkan/vulkan.h>
#include <vector>
#include <string>
#include <string_view>
#include <array>
#include <sstream>
#include <optional>
#include "vk_depth_buffer.hpp"
#include "vk_exceptions.hpp"
#include "vk_memory_layout.hpp"

#ifdef DEBUG_MODE
const bool enableValidationLayers = true;
#else
const bool enableValidationLayers = false;
#endif

std::ostream& operator<<(std::ostream& os, VkResult res);

VkApplicationInfo Get_Application_Info(char const* app_name) noexcept;

VKAPI_ATTR VkBool32 VKAPI_CALL Debug_Callback_Straightforward(VkDebugReportFlagsEXT flags,
    VkDebugReportObjectTypeEXT objType,
    uint64_t obj,
    size_t location,
    int32_t code,
    char const* layerPrefix,
    char const* msg,
    void* userData);

struct Vk_Buffer_Info {
    VkBuffer buffer { VK_NULL_HANDLE };
    std::optional<Vk_Buffer_Memory_Slice> memory_slice {};  // optional for deferred construction.

    void Bind_Buffer_Memory(VkDevice logical_device);
};

struct Vk_Image_Info {
    VkImage image { VK_NULL_HANDLE };
    //! A VkImageView for the whole image.
    VkImageView image_view { VK_NULL_HANDLE };
    std::optional<Vk_Image_Memory_Slice> memory_slice {};  // optional for deferred construction.

    void Bind_Image_Memory(VkDevice logical_device);
    void Destroy(VkDevice logical_device);
};

struct Vk_State {
    Vk_State(SDL_Window* window) :
	    window { window }
    {
	    this->Initialize_State();
    }
    ~Vk_State();

    void Draw_Frame();
    void Wait_End_Asynchronous_Operations();
    void Notify_Window_Resized();
    void Recreate_Swap_Chain();
private:
    SDL_Window* window { nullptr };
    VkInstance instance {};
    VkDebugUtilsMessengerEXT callback {};

    VkSurfaceKHR surface {};

    VkSwapchainKHR swap_chain {};
    std::vector<VkImage> swap_chain_images {};
    std::vector<VkImageView> swap_chain_image_views {};
    VkFormat swap_chain_image_format {};
    VkExtent2D swap_chain_extent {};

    struct {
        VkPipelineLayout pipeline_layout_blades {};
        VkPipelineLayout pipeline_layout_ground {};
        VkPipeline graphics_pipeline_blades {};
        VkPipeline graphics_pipeline_ground {};
        Vk_Buffer_Info blades_vertex_buffer {};
        Vk_Buffer_Info ground_vertex_buffer {};
        VkDescriptorSetLayout descriptor_layout_blades {};
        VkDescriptorSetLayout descriptor_layout_ground {};
        VkDescriptorSet descriptor_set_blades {};
        VkDescriptorSet descriptor_set_ground {};
        VkDescriptorPool descriptor_pool_blades {};
        VkDescriptorPool descriptor_pool_ground {};
        Vk_Image_Info ground_grass_texture {};
    } static_patch;

    VkPhysicalDevice physical_device { VK_NULL_HANDLE };
    VkDevice logical_device {};
    VkQueue graphics_queue {};
    VkQueue presentation_queue {};
    VkRenderPass render_pass {};
    VkPipelineLayout pipeline_layout_height_maps {};
    VkPipeline graphics_pipeline_height_map_ground {};
    std::vector<VkFramebuffer> swap_chain_framebuffers {};
    VkCommandPool graphics_command_pool {};
    std::vector<VkCommandBuffer> command_buffers_render_pass_swap_chain {};
    VkSemaphore semaphore_image_available {}, semaphore_render_finished {};
    Vk_Depth_Buffer depth_buffer;

    std::vector<char const*> extensions {};

    Vk_Buffer_Info height_maps_patches_vertex_buffer {};
    Vk_Buffer_Info matrix_buffer {};
    VkDeviceMemory allocated_memory {};
    VkDescriptorSetLayout descriptor_layout_height_map_ground {};
    VkDescriptorSet descriptor_set_height_map_ground {};
    VkDescriptorPool descriptor_pool_height_map_ground {};
    VkSampler sampler_textures {};
    VkSampler sampler_height_maps {};
    std::vector<Vk_Image_Info> height_maps_textures_images_info {};
    std::vector<Vk_Image_Info> height_maps_images_info {};

    Vk_Memory_Layout memory_layout {};

    constexpr static VkFormat format_rgba_textures { VK_FORMAT_R8G8B8A8_UNORM };

    void Initialize_State();
    void Get_All_Required_Instance_Extensions(SDL_Window* window);
    void Verify_Vk_API_Version_Suitable();
    void Create_Vk_Instance();
    void Setup_Debug_Callback();
    void Select_Physical_Device();
    void Setup_Logical_Device();
    void Setup_Swap_Chain();
    void Setup_Swap_Chain_Image_Views();
    void Setup_Descriptor_Pools();
    void Setup_Static_Patch_Ground_Descriptor_Pool();
    void Setup_Height_Map_Ground_Descriptor_Pool();
    void Setup_Graphics_Pipelines();
    void Setup_Static_Patch_Blades_Graphics_Pipeline();
    void Setup_Static_Patch_Ground_Graphics_Pipeline();
    void Setup_Height_Map_Graphics_Pipeline();
    void Setup_Render_Pass();
    void Setup_Framebuffers();
    void Setup_Graphics_Command_Pool();
    void Setup_Command_Buffers();
    void Add_Draw_Commands_For_Static_Patch(size_t command_buffer_index);
    void Add_Draw_Commands_For_Height_Maps_Rendering(size_t command_buffer_index);
    void Setup_Semaphores();
    void Clean_Up_Swap_Chain();
    void Setup_Vertex_Buffers();
    void Setup_Matrix_Buffer();
    void Allocate_Device_Memory();
    void Setup_Samplers();
    void Setup_Texture_Image_Views();
    void Setup_Texture_Images();
    void Copy_Textures_To_Images();
    void Setup_Depth_Buffer();
    void Copy_Uniform_Matrix_To_Buffer();

    Vk_Buffer_Info Create_Buffer(unsigned int byte_size, VkBufferUsageFlagBits usage, bool exclusive = true);
};

#endif // VULKAN_GRASS_VK_HPP
