#ifndef VULKAN_GRASS_VK_MEMORY_LAYOUT
#define VULKAN_GRASS_VK_MEMORY_LAYOUT

#include <vulkan/vulkan.h>
#include <vector>
#include <span>

struct Vk_Memory_Layout;
struct Memory_Allocation_Info;

template <bool Host_Mappable>
struct Vk_Memory_Slice {
public:
    unsigned int Get_Offset() const;
    unsigned int Get_Size() const;
    VkDeviceMemory Get_VkDeviceMemory() const;

    char* Get_Host_Mapped_Memory_Pointer() requires (Host_Mappable);
    std::span<char> Get_Host_Mapped_Memory_Span() requires (Host_Mappable);

private:
    unsigned int offset {};
    unsigned int size {};
    Memory_Allocation_Info* memory;
protected:
    Vk_Memory_Slice(unsigned int offset, unsigned int size, Memory_Allocation_Info&);
};

//! A reference to a host-visible memory portion allocated to a particular buffer.
struct Vk_Buffer_Memory_Slice: public Vk_Memory_Slice<true> {
private:
    using Vk_Memory_Slice::Vk_Memory_Slice;
    friend Vk_Memory_Layout;
};

//! A reference to a GPU-optimal memory portion allocated to a particular image.
struct Vk_Image_Memory_Slice: public Vk_Memory_Slice<false> {
private:
    using Vk_Memory_Slice::Vk_Memory_Slice;
    friend Vk_Memory_Layout;
};

struct Vk_Memory_Already_Allocated {
};

struct Memory_Allocation_Info {
    VkDeviceMemory memory { VK_NULL_HANDLE };
    /** @brief The smallest possible offset for the next buffer/image to be allocated.
	@details The next buffer/image's offset computation will have to take alignment into account, and thus may be greater.
    */
    unsigned int offset_next_object { 0 }; // Starting at zero since vkAllocateMemory already guarantees the strongest alignment possible.
    //! Only usable for host-visible memory types.
    char* host_mapped_pointer { nullptr };
    Vk_Memory_Layout* parent_memory_layout { nullptr };
};

/** @brief The Vk_Memory_Layout class is a pseudo-allocator, and allows the management of device and host-visible memory resources.
    @details It only works in a static context (i.e.: where the count and size of resources is known ahead of time, and all resources can be allocated at the beginning of the program).
             Only one memory allocation is made for all buffers, and likewise, only one for textures.
 */
struct Vk_Memory_Layout {
public:
    Vk_Memory_Layout() = default;
    //! To be called when the devices are initialized properly.
    void Set_Devices(VkPhysicalDevice physical_device, VkDevice logical_device);
    Vk_Memory_Layout(Vk_Memory_Layout&&) = delete;
    Vk_Memory_Layout& operator=(Vk_Memory_Layout&&) = delete;

    [[nodiscard]]
    Vk_Buffer_Memory_Slice Reserve_Vertex_Array(VkBuffer buffer);
    [[nodiscard]]
    Vk_Image_Memory_Slice Reserve_Texture(VkImage image);
    /** @brief Allocate all host and device memory.
        @note No more vertex arrays/textures may be reserved past this member function call, as the layout will be fixed in stone for the remainder of the program.
        @throws Vk_Memory_Already_Allocated If the member function was already called.
    */
    void Allocate_Memory();
    //! To be called at the end of the program, to deallocate device and host-visible memory.
    void Deallocate();
private:
    VkPhysicalDevice physical_device { VK_NULL_HANDLE };
    VkDevice logical_device { VK_NULL_HANDLE };
    bool memory_allocated { false };

    Memory_Allocation_Info vertex_array_memory { .parent_memory_layout = this };
    Memory_Allocation_Info texture_memory { .parent_memory_layout = this };

    void Allocate_Memory_For_Type(Memory_Allocation_Info& memory_type_object, int memory_type_index);

    friend Vk_Memory_Slice<true>;
};

template <bool Host_Mappable>
unsigned int Vk_Memory_Slice<Host_Mappable>::Get_Offset() const {
    return this->offset;
}

template <bool Host_Mappable>
unsigned int Vk_Memory_Slice<Host_Mappable>::Get_Size() const {
    return this->size;
}

template <bool Host_Mappable>
VkDeviceMemory Vk_Memory_Slice<Host_Mappable>::Get_VkDeviceMemory() const {
    return this->memory->memory;
}


#endif // VULKAN_GRASS_VK_MEMORY_LAYOUT
