#ifndef VULKAN_GRASS_VK_SHADER_HPP
#define VULKAN_GRASS_VK_SHADER_HPP

#include <vulkan/vulkan.h>
#include <vector>
#include <optional>
#include <array>
#include <span>

std::optional<VkShaderModule> Create_Shader_Module(VkDevice device, const unsigned int* bytecode, size_t size_shader);

inline VkPipelineShaderStageCreateInfo Get_VkPipelineShaderStageCreateInfo(VkShaderModule shader_module, VkShaderStageFlagBits stage, char const* entry_point_name = "main") {
    return {
        .sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
        .pNext = nullptr,
        .flags = 0,
        .stage = stage,
        .module = shader_module,
        .pName = entry_point_name,
        .pSpecializationInfo = nullptr
    };
}

inline VkPipelineShaderStageCreateInfo Get_VkPipelineShaderStageCreateInfo_Compute_Shader(VkShaderModule shader_module, char const* entry_point_name = "main") {
    return Get_VkPipelineShaderStageCreateInfo(shader_module, VK_SHADER_STAGE_COMPUTE_BIT, entry_point_name);
}

inline VkPipelineShaderStageCreateInfo Get_VkPipelineShaderStageCreateInfo_Vertex_Shader(VkShaderModule shader_module, char const* entry_point_name = "main") {
    return Get_VkPipelineShaderStageCreateInfo(shader_module, VK_SHADER_STAGE_VERTEX_BIT, entry_point_name);
}

inline VkPipelineShaderStageCreateInfo Get_VkPipelineShaderStageCreateInfo_Tessellation_Control_Shader(VkShaderModule shader_module, char const* entry_point_name = "main") {
    return Get_VkPipelineShaderStageCreateInfo(shader_module, VK_SHADER_STAGE_TESSELLATION_CONTROL_BIT, entry_point_name);
}

inline VkPipelineShaderStageCreateInfo Get_VkPipelineShaderStageCreateInfo_Tessellation_Evaluation_Shader(VkShaderModule shader_module, char const* entry_point_name = "main") {
    return Get_VkPipelineShaderStageCreateInfo(shader_module, VK_SHADER_STAGE_TESSELLATION_EVALUATION_BIT, entry_point_name);
}

inline VkPipelineShaderStageCreateInfo Get_VkPipelineShaderStageCreateInfo_Fragment_Shader(VkShaderModule shader_module, char const* entry_point_name = "main") {
    return Get_VkPipelineShaderStageCreateInfo(shader_module, VK_SHADER_STAGE_FRAGMENT_BIT, entry_point_name);
}

/*** A wrapper over a (vertex, fragment) shader pair, that can give the corresponding list of VkPipelineShaderStageCreateInfo.
    This class helps remove some redundancy in the Vulkan pipelines creation functions. */
struct Basic_Pipeline_Shaders_Set {
public:
    //Basic_Pipeline_Shaders_Set(VkDevice logical_device, VkShaderModule vertex_shader, VkShaderModule fragment_shader_module) :
    Basic_Pipeline_Shaders_Set(VkShaderModule vertex_shader, VkShaderModule fragment_shader_module, VkDevice logical_device) :
        logical_device { logical_device }, vertex_shader_module { vertex_shader }, fragment_shader_module { fragment_shader_module }
    {}
    ~Basic_Pipeline_Shaders_Set() {
        this->Destroy();   // Hopefully devirtualization kicks in here. Although this wrapper object is not performance-critical.
    }

    virtual void Destroy() {
        this->Destroy_All_Shaders(this->vertex_shader_module, this->fragment_shader_module);
    }

    std::array<VkPipelineShaderStageCreateInfo, 2> Get_Array_VkPipelineShaderStageCreateInfo(char const* pName = "main") const {
        return { Get_VkPipelineShaderStageCreateInfo_Vertex_Shader(this->vertex_shader_module, pName),
                  Get_VkPipelineShaderStageCreateInfo_Fragment_Shader(this->fragment_shader_module, pName) };
    }

private:
    VkDevice logical_device;
    VkShaderModule vertex_shader_module { VK_NULL_HANDLE };
    VkShaderModule fragment_shader_module { VK_NULL_HANDLE };

protected:
    template<typename... T>
    void Destroy_All_Shaders(T&... shader_modules) {
        (vkDestroyShaderModule(this->logical_device, shader_modules, nullptr), ...);
        ((shader_modules = VK_NULL_HANDLE) && ...);
    }
};

//! A wrapper over a (vertex, tessellation control, tessellation evaluation, fragment) shaders quadruple.
struct Pipeline_Shaders_Set_With_Tessellation: public Basic_Pipeline_Shaders_Set {
public:
    template<typename... T>
    Pipeline_Shaders_Set_With_Tessellation(VkShaderModule tessellation_control_shader, VkShaderModule tessellation_evaluation_module, T... t) :
        Basic_Pipeline_Shaders_Set { t... },
        tessellation_control_shader_module { tessellation_control_shader }, tessellation_evaluation_shader_module { tessellation_evaluation_module }
    {}

    void Destroy() override {
        this->Basic_Pipeline_Shaders_Set::Destroy();
        this->Destroy_All_Shaders(this->tessellation_control_shader_module, this->tessellation_evaluation_shader_module);
    }

    auto Get_Array_VkPipelineShaderStageCreateInfo(char const* pName = "main") const {
        auto parent_array { this->Basic_Pipeline_Shaders_Set::Get_Array_VkPipelineShaderStageCreateInfo(pName) };
        std::array<VkPipelineShaderStageCreateInfo, parent_array.size() + 2> array {};
        std::copy(parent_array.begin(), parent_array.end(), array.begin());
        const size_t start_index { parent_array.size() };
        array[start_index] = Get_VkPipelineShaderStageCreateInfo_Tessellation_Control_Shader(this->tessellation_control_shader_module, pName);
        array[start_index + 1] = Get_VkPipelineShaderStageCreateInfo_Tessellation_Evaluation_Shader(this->tessellation_evaluation_shader_module, pName);
        return array;
    }

private:
    VkShaderModule tessellation_control_shader_module { VK_NULL_HANDLE };
    VkShaderModule tessellation_evaluation_shader_module { VK_NULL_HANDLE };
};

using Shader_SPIRV_View = std::span<const unsigned int>;

Basic_Pipeline_Shaders_Set Create_Shader_Pipeline_Set(VkDevice logical_device, Shader_SPIRV_View vertex_spirv, Shader_SPIRV_View fragment_spirv, char const* pipeline_name);
Pipeline_Shaders_Set_With_Tessellation Create_Shader_Pipeline_Set(VkDevice logical_device, Shader_SPIRV_View vertex_spirv,
                                                                  Shader_SPIRV_View tessellation_control_spirv, Shader_SPIRV_View tessellation_evaluation_spirv,
                                                                  Shader_SPIRV_View fragment_spirv, char const* pipeline_name);

#endif // VULKAN_GRASS_VK_SHADER_HPP
