#ifndef VULKAN_GRASS_VK_TEXTURE_COPY
#define VULKAN_GRASS_VK_TEXTURE_COPY

#include <vulkan/vulkan.h>
#include <vector>
#include <span>
#include <cstdint>
#include "vk_queuefamily.hpp"

/* This class is tasked with automatising the copying of a set of textures from CPU memory to GPU memory (in a VkImage and the device memory backing it),
   so that they are ready to be used as a shader resource.
   It is meant for use within a single thread.
   \note There is no need to copy non-whole images in this project, or any elaborate formats, so this is not allowed in the interface.  */
class Vulkan_Texture_Copy_Job_Scheduler {
public:
    Vulkan_Texture_Copy_Job_Scheduler() = default;
    Vulkan_Texture_Copy_Job_Scheduler(Vulkan_Texture_Copy_Job_Scheduler const&) = delete;
    Vulkan_Texture_Copy_Job_Scheduler& operator=(Vulkan_Texture_Copy_Job_Scheduler const&) = delete;
    void Set_Logical_Device(VkDevice device);
    void Set_Device(VkPhysicalDevice, VkDevice, VkSurfaceKHR);
    /*** Add a new texture to copy to the current batch.
     * \param image_bytes The contents of the image's pixels. The memory of the span must still be valid up to the call of \ref Start_Copying_Images.
     */
    void Add_Image_To_Transfer(std::span<const char> image_bytes, VkImage target_image, size_t width, size_t height, size_t depth = 1, bool sharing_exclusive = true);
    void Start_Copying_Images();
    //! Wait for the current batch of textures to be fully copied to VRAM. A new batch can be submitted after that.
    void Wait_For_Copying_To_End();
    //! Release the resources held for image copying. This must not be called before the textures are fully copied (before a call to \ref Wait_For_Copying_To_End).
    void Destroy();
private:
    struct Image_Copy_Job_Information {
        std::span<const char> image_bytes {};
        VkImage target_image {};
        size_t width {};
        size_t height {};
        size_t depth {};
        bool sharing_exclusive { true };
    };
    VkDevice logical_device {};
    VkPhysicalDevice physical_device {};
    VkSurfaceKHR surface {};
    VkDeviceMemory memory_buffer_source { VK_NULL_HANDLE };
    VkCommandPool command_pool_copy {};
    //! The command buffer where the "copy image data to memory" commands will be recorded.
    VkCommandBuffer command_buffer_copy {};
    VkCommandPool command_pool_transfer_ownership {};
    //! The command buffer for the graphics queue to take ownership of the images.
    VkCommandBuffer command_buffer_transfer_ownership {};
    //! The queue on which the copying commands will be executed.
    VkQueue queue_copy {};
    //! The queue from the graphics queue family that will take ownership of all exclusive VkImages after copying image data.
    VkQueue queue_graphics {};
    //! A fency to notify the host that all images have been copied to device memory.
    VkFence end_of_copy_fence {};
    VkSemaphore semaphore_synchronization_copy_and_transfer_ownership { VK_NULL_HANDLE };
    std::vector<VkBuffer> buffers_source_pictures {};
    //! The size of the chunk of memory allocated to hold a copy of all images' pixel data.
    size_t allocated_memory_size { 0 };
    //! The pointer to host memory which will be used as the source of a copy-to-image command.
    void* host_mapped_memory_pointer { nullptr };
    std::vector<Image_Copy_Job_Information> images_to_copy_information {};
    bool transfer_and_graphics_queue_are_same { false };
    Queue_Family_Indices indices {};

    void Create_Transfer_Source_Buffers_For_All_Images();
    std::vector<size_t> Get_Offsets_Inside_Buffer_For_All_Images() const;
    //! Allocate a larger bunch of memory if the current batch requires more memory than previously needed (or if no memory was allocated yet, for the first batch).
    void Reallocate_More_Memory(size_t size_new_block);
};

#endif // VULKAN_GRASS_VK_TEXTURE_COPY