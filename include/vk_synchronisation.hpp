#ifndef VULKAN_GRASS_VK_SYNCHRONISATION
#define VULKAN_GRASS_VK_SYNCHRONISATION

#include "vulkan/vulkan.h"

VkFence Create_New_VkFence(VkDevice device, bool create_signalled = false);

#endif // VULKAN_GRASS_VK_SYNCHRONISATION