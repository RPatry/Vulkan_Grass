#ifndef VULKAN_GRASS_SCENE_HPP
#define VULKAN_GRASS_SCENE_HPP

#include <utility>
#include <vector>
#include <glm/glm.hpp>
#include "utils.hpp"

//! Stores data related to an early test grass patch with static grade blades.
namespace scene::static_patch {
struct Grass_Blade_Vertex {
    glm::vec3 position;
    glm::vec3 color;  // x => red, y => green, z => blue
};
extern std::vector<Grass_Blade_Vertex> grass_blade_vertices;
struct Grass_Ground_Vertex {
    glm::vec3 position;
    glm::vec2 texture_coordinate;
};
extern std::vector<Grass_Ground_Vertex> ground_vertices;

void Create_Static_Patch();
}  // end of namespace static_patch

namespace scene {

extern glm::mat4 view_matrix, projection_matrix;

void Update_Time_Spent();
void Load_Textures();
void Create_Patches();

void Advance_Camera();
void Move_Camera_Backwards();
void Advance_Camera_Left();
void Advance_Camera_Right();
void Move_Camera_Left();
void Move_Camera_Right();
void Move_Camera_Up();
void Move_Camera_Down();
void Reset_Camera();

void Set_Window_Size(int width, int height);

std::pair<unsigned int, unsigned int> Get_Texture_Size();
unsigned int Get_Total_Size_Vertex_Data();
unsigned int Get_Total_Size_Textures();
unsigned int Get_Memory_Offset_For_Texture();
void Copy_Texture_Data(char* pointer_host_memory);
std::span<const char> Get_Ground_Texture_Data();
std::vector<std::span<const char>> Get_Height_Maps_Texture_Pixels();
std::vector<std::pair<unsigned int, unsigned int>> Get_Height_Maps_Textures_Sizes();
std::vector<std::span<const glm::vec3>> Get_Height_Map_Tessellation_Patches();
std::vector<std::span<const uint8_t>> Get_Height_Maps();
}

#endif // VULKAN_GRASS_SCENE_HPP
