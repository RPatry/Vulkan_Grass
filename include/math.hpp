#ifndef VULKAN_GRASS_MATH_HPP
#define VULKAN_GRASS_MATH_HPP

#include <concepts>

namespace math {

template<typename T>
requires std::integral<T> or std::floating_point<T>
constexpr T pow(T val, unsigned int power) {
    if (power == 0) {
        return static_cast<T>(1);
    }
    else {
        return val * math::pow(val, power - 1);
    }
}

constexpr unsigned long int fact(unsigned long int x) {
    if (x < 2) {
        return 1;
    }
    else {
        return x * fact(x - 1);
    }
}

template<std::floating_point T>
constexpr T cos(T x, unsigned int steps=5) {
    // Small Taylor series, for a fast but imprecise result
    // Special cases like +Inf are not taken into account.
    int sign { 1 };
    T total { 0 };
    for (unsigned int i = 0; i < steps; i++) {
        const T current_power { math::pow(x, i * 2) };
        const auto current_fact = math::fact(i * 2);
        total = total + sign * (current_power / current_fact);
        sign *= -1;
    }
    return total;
}

template<std::floating_point T>
constexpr T sin(T x, unsigned int steps=5) {
    int sign { 1 };
    T total { 0 };
    for (unsigned int i = 0; i < steps; i++) {
        const T current_power { math::pow(x, i * 2 + 1) };
        const auto current_fact = math::fact(i * 2 + 1);
        total = total + sign * (current_power / current_fact);
        sign *= -1;
    }
    return total;
}

template<std::floating_point T>
constexpr T ceil(T x) {
    const auto x_as_integer { static_cast<unsigned long long int>(x) };
    return static_cast<T>(x_as_integer + 1);
}

inline unsigned int Get_Next_Aligned_Offset(unsigned int base_offset, unsigned int alignment) {
    const unsigned int byte_count_to_previous_aligned_offset { base_offset % alignment };
    if (byte_count_to_previous_aligned_offset == 0) {
        return base_offset;
    }
    return base_offset + (alignment - byte_count_to_previous_aligned_offset);
}
}

#endif // VULKAN_GRASS_MATH_HPP
