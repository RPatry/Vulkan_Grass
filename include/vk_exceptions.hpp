#ifndef VULKAN_GRASS_VK_INSTANCE_EXCEPTION
#define VULKAN_GRASS_VK_INSTANCE_EXCEPTION

#include <string_view>
#include <sstream>
#include <vulkan/vulkan.h>

//! The main exception type for when a Vulkan API call goes wrong.
struct Vulkan_API_Call_Error {
    Vulkan_API_Call_Error(std::string_view string, VkResult result) :
        error_message { this->Make_Error_Message_With_Result(string, result) }
    {}
    Vulkan_API_Call_Error(std::string_view string) :
        error_message { string }
    {}
    Vulkan_API_Call_Error(Vulkan_API_Call_Error const&) = default;
    const std::string error_message;
private:
    std::string Make_Error_Message_With_Result(std::string_view sv, VkResult result) const {
        std::ostringstream stream;
        stream << sv << result;
        return stream.str();
    }
};

namespace {
inline void Throw_On_VkResult_Error(std::string_view message, VkResult result) {
    if (result != VK_SUCCESS) {
        throw Vulkan_API_Call_Error { std::string { message } + ": ", result };
    }
}
}

#endif // VULKAN_GRASS_VK_INSTANCE_EXCEPTION
