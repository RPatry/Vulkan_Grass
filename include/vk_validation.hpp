#ifndef VULKAN_GRASS_VK_VALIDATION_HPP
#define VULKAN_GRASS_VK_VALIDATION_HPP

#include <vulkan/vulkan.h>
#include <string>
#include <iostream>
#include <vector>

const std::vector<char const*> validationLayers = {
    "VK_LAYER_KHRONOS_validation"
};

struct Validation_Layer {
    Validation_Layer(std::string const& name, std::string const& descr) :
	name { name }, description { descr }
    {}
    const std::string name {};
    const std::string description {};
};

std::ostream& operator<<(std::ostream& os, Validation_Layer const& vl);

bool Check_Layer_Validation_Support();

std::vector<Validation_Layer> Get_Validation_Layers_Available();

void Test_Show_Validation_Layers() noexcept;

#endif // VULKAN_GRASS_VK_VALIDATION_HPP
