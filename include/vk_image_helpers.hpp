#ifndef VULKAN_GRASS_VK_2D_TEXTURES_HPP
#define VULKAN_GRASS_VK_2D_TEXTURES_HPP

#include "vulkan/vulkan.hpp"

//! Create a 2D, exclusive VkImage ready to have data transferred to it.
VkImage Create_Simple_2D_VkImage(VkDevice logical_device, size_t width, size_t height, VkFormat format = VK_FORMAT_R8G8B8A8_UNORM);
VkImageView Create_Simple_2D_VkImageView(VkDevice logical_device, VkImage image, VkFormat format = VK_FORMAT_R8G8B8A8_UNORM);
VkDescriptorImageInfo Get_VkDescriptorImageInfo_For_Shader(VkImageView image_view, VkSampler sampler);

#endif // VULKAN_GRASS_VK_2D_TEXTURES_HPP