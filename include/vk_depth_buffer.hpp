#ifndef VULKAN_GRASS_VK_DEPTH_BUFFER
#define VULKAN_GRASS_VK_DEPTH_BUFFER

#include <vulkan/vulkan.h>
#include <SDL2/SDL.h>

struct Vk_Depth_Buffer {
    Vk_Depth_Buffer()
    {}
    Vk_Depth_Buffer(VkPhysicalDevice physical_device, VkDevice logical_device, SDL_Window* window);

    void Recreate_Buffer();
    VkImageView Get_Image_View() const;
    void Clean_Resources();
private:
    VkPhysicalDevice physical_device;
    VkDevice logical_device;
    SDL_Window* window { nullptr };
    VkImage depth_buffer_image;
    VkImageView depth_buffer_image_view;
    VkDeviceMemory depth_buffer_memory;  // The depth buffer gets its own dedicated allocation
    unsigned int allocation_size { 0 };

    void Create_Image();
    void Reallocate_Memory_If_Required();
    void Allocate_Memory();
    void Create_Image_View();
    void Bind_Image_To_Memory();
};

#endif // VULKAN_GRASS_VK_DEPTH_BUFFER
