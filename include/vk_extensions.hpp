#ifndef VULKAN_GRASS_VK_EXTENSIONS_HPP
#define VULKAN_GRASS_VK_EXTENSIONS_HPP

#include <vulkan/vulkan.h>
#include <vector>
#include <set>
#include <string>
#include <concepts>

void Test_Show_Global_Extensions_Properties() noexcept;

std::vector<std::string> Get_All_Extensions_Names_For_Device(VkPhysicalDevice device);

//! Check if a Vk device has all the extensions we require.
bool Check_Device_Has_All_Extensions(VkPhysicalDevice device);

template<typename T>
requires std::same_as<T, std::string> or std::same_as<T, char const*>
const std::vector<T> device_extensions {
    T(VK_KHR_SWAPCHAIN_EXTENSION_NAME),
    T("VK_KHR_create_renderpass2"),
    T("VK_KHR_multiview"),
    T("VK_KHR_maintenance2"),
    T("VK_KHR_separate_depth_stencil_layouts"),
    T("VK_KHR_shader_non_semantic_info")
};

#endif // VULKAN_GRASS_VK_EXTENSIONS_HPP
