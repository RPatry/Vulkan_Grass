#ifndef VULKAN_GRASS_VK_SWAPCHAIN_HPP
#define VULKAN_GRASS_VK_SWAPCHAIN_HPP

#include <vulkan/vulkan.h>
#include <vector>

struct Swap_Chain_Support_Details {
    VkSurfaceCapabilitiesKHR capabilities {};
    std::vector<VkSurfaceFormatKHR> formats {};
    std::vector<VkPresentModeKHR> present_modes {};
};

Swap_Chain_Support_Details Get_Swap_Chain_Support_Details(VkPhysicalDevice, VkSurfaceKHR);

VkSurfaceFormatKHR Choose_Swap_Surface_Format(std::vector<VkSurfaceFormatKHR> const& formats);

VkPresentModeKHR Choose_Swap_Present_Mode(std::vector<VkPresentModeKHR> const& modes);

VkExtent2D Choose_Swap_Extent(VkSurfaceCapabilitiesKHR const&, uint32_t width, uint32_t height);

#endif // VULKAN_GRASS_VK_SWAPCHAIN_HPP
