#ifndef VULKAN_GRASS_SHADERS_HPP
#define VULKAN_GRASS_SHADERS_HPP

extern "C" {
    extern const unsigned int Static_Patch_Blade_Frag_Shader [];
    extern const unsigned int Static_Patch_Blade_Vert_Shader [];
    extern const unsigned int Static_Patch_Ground_Frag_Shader [];
    extern const unsigned int Static_Patch_Ground_Vert_Shader [];
    extern const unsigned int Static_Patch_Blade_Frag_Shader_Size, Static_Patch_Blade_Vert_Shader_Size;
    extern const unsigned int Static_Patch_Ground_Frag_Shader_Size, Static_Patch_Ground_Vert_Shader_Size;

    extern const unsigned int Height_Map_Vertex_Shader [];
    extern const unsigned int Height_Map_TessellationControl_Shader [];
    extern const unsigned int Height_Map_TessellationEvaluation_Shader [];
    extern const unsigned int Height_Map_Fragment_Shader [];
    extern const unsigned int Height_Map_Vertex_Shader_Size;
    extern const unsigned int Height_Map_TessellationControl_Shader_Size;
    extern const unsigned int Height_Map_TessellationEvaluation_Shader_Size;
    extern const unsigned int Height_Map_Fragment_Shader_Size;
}

#endif // VULKAN_GRASS_SHADERS_HPP
