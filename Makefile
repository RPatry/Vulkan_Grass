##### Functions

# Returns a list of all files/directories whose name match a pattern. Search recursively in each subdirectory.
rec_wildcard = $(wildcard $(1)$(2)) $(foreach d,$(wildcard $(1)*), $(call rec_wildcard,$(d)/,$(2)))

# Returns a list of all-subdirectories of a directory (including itself)
subdirs = $(sort $(dir $(call rec_wildcard,$(1)/,*)))

ifeq ($(OS),Windows_NT)
IS_WINNT := "true"
else
IS_WINNT :=
endif
# Returns a path ready to use for shell commands: essentially just turns '/' into '\' for Windows only
get_os_path = $(if $(IS_WINNT), $(subst /,\,$(strip $(1))), $(1))

check_directory_exists = $(wildcard $(1))

## There are three major targets: clean, debug (for debugging) and release (optimised executable)
## If no target is passed, debug and release will be built.

PATH_MINGW := C:\MinGW
VK_VERSION := 1.3.231.0
VK_SDK_PATH := C:\VulkanSDK
PATH_VK := $(VK_SDK_PATH)/$(VK_VERSION)
SRCDIR := src
INCDIR := include
SPIRV_COMPILER := $(PATH_VK)/Bin/glslangValidator
CXX := g++
INC := -I./$(INCDIR) -I$(PATH_MINGW)\include -I"$(VK_SDK_PATH)/$(VK_VERSION)/Include"
LDLIBS := -lm -lSDL2 -ldinput8 -ldxguid -ldxerr8 -luser32 -lgdi32 -lole32 -loleaut32 -lshell32 -limm32 -lwinmm -lversion -lSetupAPI -lmingw32 -lSDL2main -luuid "$(PATH_VK)/Lib/vulkan-1.lib" -lpng -lz
CXXFLAGS := -std=c++23 -save-temps=obj -Wall -Wextra -Werror $(INC) -Wno-volatile # -Wno-volatile is needed for GLM version 0.9.9.8
RFLAGS := -O2 -march=native
DFLAGS := -g -DDEBUG_MODE
DEBUG_DIR := debug
RELEASE_DIR := release
SRCSUBDIRS := $(call subdirs,$(SRCDIR))
SHADERDIR := shaders
DEPSDIR := .deps
DEPSDIRS := $(DEPSDIR) $(addprefix  .deps/$(DEBUG_DIR)/, $(SRCSUBDIRS))   $(addprefix .deps/$(RELEASE_DIR)/, $(SRCSUBDIRS))
OBJDIRS := obj $(addprefix  obj/$(DEBUG_DIR)/, $(SRCSUBDIRS)) $(addprefix obj/$(RELEASE_DIR)/, $(SRCSUBDIRS))
BINDIRS := bin bin/$(DEBUG_DIR) bin/$(RELEASE_DIR) bin/$(SHADERDIR)

ifeq ($(OS),Windows_NT)
EXEC := VkGrass.exe
else
EXEC := VkGrass
endif

SRC := $(call rec_wildcard,$(SRCDIR)/,*.cpp)
ASM := $(call rec_wildcard,$(SRCDIR)/,*.s)
DOBJS := $(SRC:$(SRCDIR)/%.cpp=obj/$(DEBUG_DIR)/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/$(DEBUG_DIR)/$(SRCDIR)/%.o)
ROBJS := $(SRC:$(SRCDIR)/%.cpp=obj/$(RELEASE_DIR)/$(SRCDIR)/%.o) $(ASM:$(SRCDIR)/%.s=obj/$(RELEASE_DIR)/$(SRCDIR)/%.o)
OBJS := $(DOBJS) $(ROBJS)
SHADERS := $(wildcard $(SHADERDIR)/*.vert) $(wildcard $(SHADERDIR)/*.frag) $(wildcard $(SHADERDIR)/*.tesc) $(wildcard $(SHADERDIR)/*.tese) $(wildcard $(SHADERDIR)/*.geom)
SPIRVOUT := $(SHADERS:$(SHADERDIR)/%=bin/$(SHADERDIR)/%.spv)

DDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/$(DEBUG_DIR)/$(SRCDIR)/%.cpp.d)
RDEPS := $(SRC:$(SRCDIR)/%.cpp=.deps/$(RELEASE_DIR)/$(SRCDIR)/%.cpp.d)
DEPS := $(DDEPS) $(RDEPS)

OUTASM := $(SRC:%.cpp=obj/$(RELEASE_DIR)/%.s) $(SRC:%.cpp=obj/$(DEBUG_DIR)/%.s)
II := $(SRC:%.cpp=obj/$(RELEASE_DIR)/%.ii) $(SRC:%.cpp=obj/$(DEBUG_DIR)/%.ii)

ALL_OUTPUT_DIRS := $(DEPSDIRS) $(BINDIRS) $(OBJDIRS)
ifeq ($(OS),Windows_NT)
# This creates a list of directories that need to be created.
ALL_OUTPUT_DIRS_MKDIR := $(foreach dir,$(strip $(ALL_OUTPUT_DIRS)),$(if $(call check_directory_exists,$(dir)),,$(call get_os_path,$(dir))))
else
ALL_OUTPUT_DIRS_MKDIR := $(ALL_OUTPUT_DIRS)
endif

.PHONY: all spirv debug release
all: setup spirv debug release

-include $(DEPS)

debug: bin/$(DEBUG_DIR)/$(EXEC)
release: bin/$(RELEASE_DIR)/$(EXEC) bin/$(EXEC)
spirv: $(SPIRVOUT)

# Re-run the .incbin directives if shaders have changed, to include them directly in the final executable
obj/$(DEBUG_DIR)/$(SRCDIR)/shaders.o: $(SPIRVOUT)
obj/$(RELEASE_DIR)/$(SRCDIR)/shaders.o: $(SPIRVOUT)

bin/$(DEBUG_DIR)/$(EXEC): $(DOBJS)
	$(CXX) -o $@ $^ $(LDLIBS)

bin/$(RELEASE_DIR)/$(EXEC): $(ROBJS)
	$(CXX) -o $@ $^ -s $(LDLIBS)

bin/$(EXEC) : $(ROBJS)
	$(CXX) -o $@ $^ -s $(LDLIBS)

bin/$(SHADERDIR)/%.spv: $(SHADERDIR)/%
	$(SPIRV_COMPILER) -V -o $@ $<

obj/$(DEBUG_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(DFLAGS) -MM -MF .deps/$(DEBUG_DIR)/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/$(RELEASE_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.cpp
	$(CXX) $(CXXFLAGS) $(RFLAGS) -MM -MF .deps/$(RELEASE_DIR)/$<.d -MT "$@" -MP -c $<
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

obj/$(DEBUG_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(DFLAGS) -c $< -o $@

obj/$(RELEASE_DIR)/$(SRCDIR)/%.o: $(SRCDIR)/%.s
	$(CXX) $(CXXFLAGS) $(RFLAGS) -c $< -o $@

bin:
	mkdir bin

$(SHADERDIR):
	mkdir $(call get_os_path,bin/$(SHADERDIR))

.PHONY: setup
setup: 
ifeq ($(OS),Windows_NT)
ifneq ($(strip $(ALL_OUTPUT_DIRS_MKDIR)),)
	mkdir $(ALL_OUTPUT_DIRS_MKDIR)
endif
else
	@mkdir -p $(ALL_OUTPUT_DIRS_MKDIR)
endif


.PHONY: clean
clean:
ifneq ($(OS),Windows_NT)
	rm -f $(OBJS) bin/$(DEBUG_DIR)/$(EXEC) bin/$(RELEASE_DIR)/$(EXEC) $(OUTASM) $(II) $(DEPS) $(SPIRVOUT)
else
	del $(call get_os_path,$(OBJS)) bin\$(DEBUG_DIR)\$(EXEC) bin\$(RELEASE_DIR)\$(EXEC) $(call get_os_path,$(OUTASM)) $(call get_os_path,$(II)) $(call get_os_path,$(DEPS)) $(call get_os_path,$(SPIRVOUT))
endif
