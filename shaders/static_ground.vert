#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 texture_UV;
layout(location = 0) out vec2 texture_UV_Frag;
layout(set = 0, binding = 0) uniform Projection_Matrix {
    mat4 projection_matrix;
} UPM;

void main() {
    gl_Position = UPM.projection_matrix * vec4(position, 1.0);
    texture_UV_Frag = texture_UV;
}
