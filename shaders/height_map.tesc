#version 450 core

layout (vertices = 4) out;
layout (location = 0) in int height_map_index[];
layout (location = 0) out int height_map_index_out[4];

void main() {
    height_map_index_out[gl_InvocationID] = height_map_index[gl_InvocationID];
    if (gl_InvocationID == 0) {
        // Using 64 as it is the minimum tessellation guaranteed to be available (this is simpler than testing
        // the maximum available by the device) and passing it to the shader).
        // A high amount of tessellation is needed as height maps terrains are big.
        float tessellation_level = 64.0f;
        gl_TessLevelInner[0] = tessellation_level;
        gl_TessLevelInner[1] = tessellation_level;

        gl_TessLevelOuter[0] = tessellation_level;
        gl_TessLevelOuter[1] = tessellation_level;
        gl_TessLevelOuter[2] = tessellation_level;
        gl_TessLevelOuter[3] = tessellation_level;
    }
    // All four patch positions are needed in the evaluation shader because the interpolated world (then device) position of each vertex of each
    // tessellated triangle will be determined there.
    gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;
}