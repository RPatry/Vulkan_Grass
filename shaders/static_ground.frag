#version 450

layout(location = 0) out vec4 out_color;
layout(location = 0) in vec2 texture_UV_Frag;
layout(set = 0, binding = 1) uniform sampler2D texture_grass;

void main() {
    out_color = texture(texture_grass, texture_UV_Frag);
}
