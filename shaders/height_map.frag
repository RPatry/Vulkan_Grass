#version 450 core

#extension GL_EXT_nonuniform_qualifier : enable

layout (constant_id = 0) const int height_map_count = 2;

layout (location = 0) out vec4 out_color;
layout (location = 0) in vec2 texture_UV_Frag;
layout (location = 1) flat in int height_map_index;

layout (set = 0, binding = 2) uniform sampler2D height_map_texture[height_map_count];

void main() {
    out_color = texture(height_map_texture[nonuniformEXT(height_map_index)], texture_UV_Frag);
}