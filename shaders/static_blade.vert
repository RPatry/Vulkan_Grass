#version 450

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 blade_color_in;
layout(location = 0) out vec3 blade_color_out;
layout(set = 0, binding = 0) uniform Projection_Matrix {
    mat4 projection_matrix;
} UPM;

out gl_PerVertex {
    vec4 gl_Position;
};

void main() {
    gl_Position = UPM.projection_matrix * vec4(position, 1.0);
    blade_color_out = blade_color_in;
}
