#version 450 core

#extension GL_EXT_nonuniform_qualifier : enable

// This shader modifies the Y position of each vertex of each tessellated triangle to reflect the terrain structure of the height map which is currently being processed.
// This is a special case of displacement mapping, with the normal vector of the terrain implicitly being parallel to the Y axis.

layout (quads) in;

layout (constant_id = 0) const int height_map_count = 2;

layout (location = 0) in int height_map_index_in[];
layout (location = 0) out vec2 texture_UV_Frag;
layout (location = 1) flat out int height_map_index;

layout (set = 0, binding = 1) uniform usampler2D height_map_heights_textures[height_map_count];
layout (set = 0, binding = 0) uniform Projection_Matrix {
    mat4 projection_matrix;
} UPM;

void main() {
    const vec4 interp1 = mix(gl_in[0].gl_Position, gl_in[1].gl_Position, gl_TessCoord.x);
    const vec4 interp2 = mix(gl_in[2].gl_Position, gl_in[3].gl_Position, gl_TessCoord.x);
    vec4 world_position = mix(interp1, interp2, gl_TessCoord.y);
    const vec2 texture_UV = gl_TessCoord.xy;
    const float raw_height = texture(height_map_heights_textures[nonuniformEXT(height_map_index_in[0])], texture_UV).x;
    const float step_height = 4.0;
    world_position.y = world_position.y + raw_height * step_height;

    gl_Position = UPM.projection_matrix * world_position;
    texture_UV_Frag = texture_UV;
    height_map_index = height_map_index_in[0];
}