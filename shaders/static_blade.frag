#version 450

layout(location = 0) out vec4 out_color;
layout(location = 0) in vec3 blade_color;

void main() {
    out_color = vec4(blade_color, 1.0);
}
