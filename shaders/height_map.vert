#version 450 core

layout (location = 0) in vec3 position;    // Patch control point
layout (location = 0) out int height_map_index;

void main() {
    gl_Position = vec4(position, 1.0);
    height_map_index = gl_VertexIndex / 4;
}
